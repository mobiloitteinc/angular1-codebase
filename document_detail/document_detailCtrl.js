'use strict';

angular.module('Lawcunae.docDetail', [])

.controller('docDetailCtrl', ['$anchorScroll', '$location', '$scope', 'docs', 'commonService', '$modal', '$sce', '$timeout', '$q', 'storage', 'API_URL', 'alertify',function($anchorScroll, $location, sc, docs, commonService, $modal, $sce, $timeout, $q, storage, API_URL, alertify) {

	sc.clipboardVal='';
	
/******Function to load javascript select2 plugin file******/
	function async(u, c) {
		var d = document, t = 'script',
			o = d.createElement(t),
			s = d.getElementsByTagName(t)[0];
		o.src = '//' + u;
		if (c) { o.addEventListener('load', function (e) { c(null, e); }, false); }
		s.parentNode.insertBefore(o, s);
	}
	
	sc.docID = commonService.stateChangeVal.value;  

	if(commonService.stateChangeVal.exist){
		storeExistingList();
	}else
		getParagraph(sc.docID);

/******Function to fetch existing document viewer data if stored locally******/
	function storeExistingList(){
		sc.clipboardVal='';
		var index=commonService.allTabsContent.document.findIndex((x) => x.keyword == commonService.stateChangeVal.value);
		if(index != -1){
			sc.tabDetail=commonService.allTabsContent.document[index].tabDetail;
			sc.clipboardVal='<a href=\"document/'+sc.tabDetail._source.id+'\">'+sc.tabDetail._source.title+'</a>';
			// getParaComments(commonService.stateChangeVal.value);
			sc.$emit('docDetailEmit', {link:sc.tabDetail._source.title, source:'indirect'});
		}else{
			getParagraph(sc.docID);
		}
	}
	
/***********Getting paragraphs from JSON file*************/
	function getParagraph(doc_id){
		sc.clipboardVal='';
		sc.tabDetail={};
		docs.get_paragraphs({"document_id":doc_id}).then(function(objS){
			if(objS.data.responseCode == 200){
				sc.tabDetail=objS.data.data[0];
				sc.clipboardVal='<a href=\"document/'+sc.tabDetail._source.id+'\">'+sc.tabDetail._source.title+'</a>';
				// commonService.allTabsContent.document.push({tabDetail:sc.tabDetail, keyword:doc_id});
				getParaComments(doc_id);
				sc.$emit('docDetailEmit', {link:sc.tabDetail._source.title, source:'indirect'});
			}
		},function(objE){
			console.log('paragraph error => '+JSON.stringify(objE));
		})
	}

/***********Initializing select2 for document tags*************/
	function autoSuggestPOP(){
		async('cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js', function() {
			$(".doc-select2").select2({
				placeholder: "Search for document tags",
				ajax: {
					url: API_URL+"documentActivity/auto_suggest",
					dataType: 'json',
					beforeSend: function (xhr) { 
						xhr.setRequestHeader("Authorization", storage.get('token'));
					},
					delay: 250,
					data: function (params) {
						return {
							suggestion: params.term
						};
					},
					processResults: function (data, params) {
					return {
							results: data.data
						};
					},
					cache: true
				},
				escapeMarkup: function (markup) { return markup; },
				minimumInputLength: 1,
				templateResult: formatRepo,
				templateSelection: formatRepoSelection
			}).trigger('change');
		});
	}

	function formatRepo (repo) {
		if (repo.loading) return "Loading terms..";

		// var markup = "<div class='select2-result-repository clearfix'>" +
		// "<div class='select2-result-repository__meta'>" +
		// 	"<div class='select2-result-repository__title'>" + repo.tag + "</div>"+
		// "</div>" +
		// "</div></div>";

		return "<div>"+repo.tag+"</div>";
	}

	function formatRepoSelection (repo) {
		return repo.tag;
	}

/***********Emit event when changes in paragraph annotation*************/
	sc.$on('updateDoc', function (event, args) {
		if(commonService.stateChangeVal.exist){
			storeExistingList();
		}else
			getParagraph(args.name);
	});

/******Open same tab when came from different document viewer******/
	sc.$on('sameTypeTab', function (event, args) {
		sc.docID = commonService.stateChangeVal.value;
		if(commonService.stateChangeVal.exist){
			storeExistingList();
		}else
			getParagraph(args.id.value);
	});

/******Get annotation counts from serevr for particular paragraphs******/
	function getParaComments(docID){
		docs.get_para_comments(docID).then(function(objS){
			// console.log('get_para_comments success => '+JSON.stringify(objS));
			if(objS.data.responseCode == 200){
				if(objS.data.data!=null){
					sc.tabDetail._source.parent_case = objS.data.data.parent_case;
					var getParaComm=objS.data.data.paragraphs;
					if(getParaComm.length==0){
						commonService.allTabsContent.document.push({tabDetail:sc.tabDetail, keyword:docID});
					}
					for(var i=0;i<getParaComm.length;i++){
						var index=sc.tabDetail._source.paragraphs.findIndex((x)=> x.paragraph_Id == getParaComm[i].paragraph_id);
						if(index!=-1){
							sc.tabDetail._source.paragraphs[index].showAnnotation=true;
							sc.tabDetail._source.paragraphs[index].annotationLen=getParaComm[i].annotations.length;
						}
						if(i == getParaComm.length-1){
							// console.log('tabDetail => '+JSON.stringify(sc.tabDetail));
							commonService.allTabsContent.document.push({tabDetail:sc.tabDetail, keyword:docID});
						}
					}
				}
			}
		},function(objE){
			console.log('get_para_comments error => '+JSON.stringify(objE));
		})
	}

/******Document printing functionality******/
	sc.printDoc = function(docID) {
		var printContents = '<div>Lawcunae</div>';
		var popupWin=window.open('modules/print/print.html?docID='+docID+'','name','height=1000,width=1000')
		popupWin.document.close();
	}

/******Subscribe printing functionality******/
	sc.subscribeDoc = function(docID) {
		var subscribe_req={"user_id":commonService.user_id,"document_subscriptions":docID};
		docs.subscribe_doc(subscribe_req).then(function(objS){
			if(objS.data.responseCode == 200){
				sc.tabDetail.subscribed=true;
			}
		},function(objE){
			console.log('subscribe_doc objE => '+JSON.stringify(objE));
		})
	}

/******Open document viewer in different tab when click on particular paragraph functionality******/
	sc.linkClick = function(link){
		// console.log('link => '+link);
		if(link.split('/')[1] == 'docm'){

		}else if(link.split('/')[1] == 'document'){
			link=link.split('/')[2];
			sc.$emit('docDetailEmit', {link:link, source:'direct'});
		}
	}

/******Graph implementation******/
	sc.makeGraph = function(docDet) {
		var modalInstance = $modal.open({
			templateUrl: 'modules/graph/graph.html',
			controller: 'graphCtrl',
			resolve: {
                docDetail: function() {
                    return docDet;
                }
            }
			// backdrop: 'static'
        });
        
        modalInstance.result.then(function (success) {
			sc.$emit('docDetailEmit', {link:success.id, source:'direct'});
        }, function () {
            // console.log('Modal dismissed');
        });
	};

/******Open side panel when click on particular annotation******/
	sc.annotationClk = function(docID, paraID){
		sc.$emit('sidePanelView', { page: 'documentTab', keyID:docID, paraID:paraID });
	}

/******Open editor when click on particular paragraph to populate data******/
	sc.paragraphClk = function(e, doc_id, paragraph, status){
		// paragraph.showEditPara=status;
		status?paragraph.edittext=paragraph.text:paragraph.text=CKEDITOR.instances[paragraph.paragraph_Id].getData();
		if(!status){
			if (!navigator.onLine) {
				alertify.error('Please check your network connection')
			} else if (paragraph.cktext == CKEDITOR.instances[paragraph.paragraph_Id].getData()) {
				alertify.error("Nothing to update")
			} else {
				var edit_req={"document_id":doc_id,"paragraph_id":paragraph.paragraph_Id,"user_id":commonService.user_id,"new_text":CKEDITOR.instances[paragraph.paragraph_Id].getData()}
				docs.edit_para(edit_req).then(function(objS){
					if(objS.data.responseCode == 200){
						paragraph.showEditPara=status;
					}
				},function(objE){
					console.log('edit_para objE => '+JSON.stringify(objE));
				})
			}
		}else {
			paragraph.showEditPara = status;
			$timeout(function() { 
				paragraph.cktext = CKEDITOR.instances[paragraph.paragraph_Id].getData(); 
			}, 500);
		}
		if(!CKEDITOR.instances[paragraph.paragraph_Id])
			CKEDITOR.replace( paragraph.paragraph_Id, {
				removeButtons: 'Source',
			} );
			
		// console.log('editor text => '+CKEDITOR.instances[paragraph.paragraph_Id].getData());
	}

/******Update functionality for paragraph******/
	sc.editParaDetail = function(status, text, source){
		// sc.tabDetail._source.showEditDetail=status;
		if(status){
			sc.tabDetail._source.showEditDetail = status;
			sc.source=source;
			if(source == 'judge' || source == 'petitioner' || source == 'respondant'){
				var tempVal=text;
				for(var i=0;i<tempVal.length;i++){
					if(i==0)
						text=tempVal[i].name;
					else
						text=text+' , '+tempVal[i].name;
				}
				sc.tabDetail._source.editDetText=text;
				sc.prev_text = text;
			}else{
				sc.tabDetail._source.editDetText=text;
				sc.prev_text = text;
			}
		}else{
			if (!navigator.onLine) {
				alertify.error('Please check your network connection')
			} else if (sc.prev_text == text) {
				alertify.error("Nothing to update")
			} else {
				if(sc.source == 'title'){
					if (text.length < 10) {
						alertify.error('Title should contain atleast 10 characters')
					} else {
						sc.tabDetail._source.title=text;
						var req={"document_id":sc.tabDetail._source.id,"new_title":text,"user_id":commonService.user_id};
						detailAPI('edit_title', req);
						sc.tabDetail._source.showEditDetail = status;
					}
				}else if(sc.source == 'date'){
					//sc.tabDetail._source.title=text;
					sc.tabDetail._source.showEditDetail = status;
				}else if(sc.source == 'court'){
					sc.tabDetail._source.courtName=text;
					var req={"document_id":sc.tabDetail._source.id, "new_court_name" : text, "user_id":commonService.user_id};
					detailAPI('edit_court', req);
					sc.tabDetail._source.showEditDetail = status;
				}else if(sc.source == 'summary'){
					if (!navigator.onLine) {
						alertify.error('Please check your network connection')
					} else if (text.length < 20) {
						alertify.error('Summary should contain atleast 20 characters')
					} else {
						sc.tabDetail._source.summary = text;
						var req = { "dcument_id": sc.tabDetail._source.id, "new_summary": text, "user_id": commonService.user_id };
						detailAPI('edit_doc_summary', req);
						sc.tabDetail._source.showEditDetail = status;
					}
				}else if(sc.source == 'judge' || sc.source == 'petitioner' || sc.source == 'respondant'){
					if(text == ''){
						if(sc.source == 'judge')
							sc.tabDetail._source.judges=[];
						else if(sc.source == 'petitioner')
							sc.tabDetail._source.petitioners=[];
						else if(sc.source == 'respondant')
							sc.tabDetail._source.respondents=[];
					}else{
						var tempVal=text.split(',');
						for(var i=0;i<tempVal.length;i++){
							if(i==0)
								text=[{name:tempVal[i]}];
							else
								text.push({name:tempVal[i]});
						}
						if(sc.source == 'judge')
							sc.tabDetail._source.judges=text;
						else if(sc.source == 'petitioner')
							sc.tabDetail._source.petitioners=text;
						else if(sc.source == 'respondant')
							sc.tabDetail._source.respondents=text;
					}
					if(sc.source == 'judge'){
						sc.tabDetail._source.judges=text;
						sc.tabDetail._source.showEditDetail = status;
					}else if(sc.source == 'petitioner'){
						sc.tabDetail._source.petitioners=text;
						var req={"document_id":sc.tabDetail._source.id,"user_id":commonService.user_id,"new_petitioners":text};
						detailAPI('edit_petitioners', req);
						sc.tabDetail._source.showEditDetail = status;
					}else if(sc.source == 'respondant'){
						sc.tabDetail._source.respondents=text;
						var req={"document_id":sc.tabDetail._source.id,"user_id":commonService.user_id,"new_respondents":text};
						detailAPI('edit_respondents', req);
						sc.tabDetail._source.showEditDetail = status;
					}
				}else if(sc.source == 'type'){
					sc.tabDetail._source.type=text;
				}
			}
		}
	}

	sc.cancelBox = function(source, paragraph){
		if(source == 'metadata')
			sc.tabDetail._source.showEditDetail=false;
		else
			paragraph.showEditPara=false;
	}

	function detailAPI(source, req){
		docs.edit_doc_detail(source, req).then(function(objS){
			if(objS.data.responseCode == 200){
				// console.log('docList =>> '+JSON.stringify(sc.docList));
			}
		},function(objE){
			console.log('edit_doc_detail objE => '+JSON.stringify(objE));
		})
	}

	sc.changeDocType = function(){
		// console.log('changed type');
	}

/***********Avoiding the restrictions on url*************/
	sc.trustAsHtml = function(html) {
		return $sce.trustAsHtml(html);
	}

/******Initialization of select2 for paragraph tags******/
	sc.tagClk = function(paragraph){
		paragraph.showDocTag?!paragraph.showDocTag:paragraph.showDocTag=true;
		$timeout(function(){
			$(".tag-select2").select2({
				placeholder: "Search for paragraph tags",
				ajax: {
					// url: 'http://172.16.6.242:8888/'+"documentActivity/para_tags_auto_suggest",
					url: API_URL+"documentActivity/para_tags_auto_suggest",
					dataType: 'json',
					// dropdownCssClass: 'tagtag',
					beforeSend: function (xhr) { 
						xhr.setRequestHeader("Authorization", storage.get('token'));
					},
					delay: 250,
					data: function (params) {
						return {
							suggestion: params.term
						};
					},
					processResults: function (data, params) {
					return {
							results: data.data
						};
					},
					cache: true
				},
				escapeMarkup: function (markup) { return markup; },
				minimumInputLength: 1,
				templateResult: formatRepo,
				templateSelection: formatRepoSelection
			}).data('select2').$dropdown.addClass('tag_popup').trigger('change');
		},500);
	}

	function formatRepo (repo) {
		if (repo.loading) return "Loading terms..";
			return "<div>"+repo.tag+"</div>";
	}

	function formatRepoSelection (repo) {
		return repo.tag;
	}

/******API integration to add new tags******/
	sc.addParaTags = function(paragraph, status){
		if(!status){
			paragraph.showDocTag?paragraph.showDocTag=false:paragraph.showDocTag=true;
			return;
		}
		var tagArr=[];
		// console.log('select2 => '+JSON.stringify($(".tag-select2").select2('data')));
		for(var i=0;i<$(".tag-select2").select2('data').length;i++){
			tagArr.push($(".tag-select2").select2('data')[i].tag);
			if(i == $(".tag-select2").select2('data').length-1){
				var req={"document_id":sc.docID, "paragraph_id":paragraph.paragraph_Id, "new_tags":tagArr};
				docs.edit_para_tag(req).then(function(objS){
					if(objS.data.responseCode == 200){
						paragraph.showDocTag?paragraph.showDocTag=false:paragraph.showDocTag=true;
					}
				},function(objE){
					console.log('edit_doc_tag objE => '+JSON.stringify(objE));
				});
			}
		}
	}

/*******Initialization of select2 for court*****/
	sc.addCourt = function(court_name){
		sc.prev_courtName = court_name;
		var modalInstance = $modal.open({
			template: '<select class="doc_court" name="state"></select><button class="btn  btn-tag-submit" ng-click="addCourtTags(\'save\')">Save</button><button class="btn  btn-tag-submit" ng-click="addCourtTags(\'cancel\')">Cancel</button>',
			backdrop: 'static',
			controller: function(API_URL, storage, $scope, $modalInstance){
				$timeout(function(){
					async('cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js', function() {
						$(".doc_court").select2({
							placeholder: "Search for Court",
							ajax: {
								url: API_URL+"documentActivity/court_auto_suggest",
								dataType: 'json',
								beforeSend: function (xhr) { 
									xhr.setRequestHeader("Authorization", storage.get('token'));
								},
								delay: 250,
								data: function (params) {
									return {
										suggestion: params.term
									};
								},
								processResults: function (data, params) {
								return {
										results: data.data
									};
								},
								cache: true
							},
							escapeMarkup: function (markup) { return markup; },
							minimumInputLength: 1,
							templateResult: formatRepoCourt,
							templateSelection: formatRepoSelectionCourt
						}).trigger('change');
					});
				},500);

				$scope.addCourtTags = function(type){
					if(type == 'cancel'){
						$modalInstance.close();
					}else{
						if (!navigator.onLine) {
							alertify.error('Please check your network connection')
						} else if(!$(".doc_court").select2('data')[0]){
							alertify.error('Please select court')
						} else if (sc.prev_courtName == $(".doc_court").select2('data')[0].court_name) {
							alertify.error("Nothing to update")
						} else {
							var req={"document_id":sc.docID, "new_court_name" : $(".doc_court").select2('data')[0].court_name, "user_id":commonService.user_id};
							detailAPI('edit_court', req);
							$modalInstance.close();
						}
					}
				}
			}
        });
	}

	function formatRepoCourt (repo) {
		if (repo.loading) return "Loading terms..";
			return "<div>"+repo.court_name+"</div>";
	}

	function formatRepoSelectionCourt (repo) {
		return repo.court_name;
	}

/******API integration to add document tags******/
	sc.addDocTag = function(){
		var modalInstance = $modal.open({
			template: '<select class="doc-select2" name="states[]" multiple="multiple"></select><button class="btn  btn-tag-submit" ng-click="addDocTags(\'save\')">Save</button><button class="btn  btn-tag-submit" ng-click="addDocTags(\'cancel\')">Cancel</button>',
			backdrop: 'static',
			controller: function(API_URL, storage, $scope, $modalInstance){
				$timeout(function(){
					async('cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js', function() {
						$(".doc-select2").select2({
							placeholder: "Search for document tags",
							ajax: {
								url: API_URL+"documentActivity/auto_suggest",
								dataType: 'json',
								beforeSend: function (xhr) { 
									xhr.setRequestHeader("Authorization", storage.get('token'));
								},
								delay: 250,
								data: function (params) {
									return {
										suggestion: params.term
									};
								},
								processResults: function (data, params) {
								return {
										results: data.data
									};
								},
								cache: true
							},
							escapeMarkup: function (markup) { return markup; },
							minimumInputLength: 1,
							templateResult: formatRepo,
							templateSelection: formatRepoSelection
						}).trigger('change');
					});
				},500);

				$scope.addDocTags = function(type){
					if(type == 'cancel'){
						$modalInstance.close();
					}else{
						var tagArr=[];
						for(var i=0;i<$(".doc-select2").select2('data').length;i++){
							tagArr.push($(".doc-select2").select2('data')[i].tag);
							if(i == $(".doc-select2").select2('data').length-1){
								var req={"document_id":sc.docID,"user_id":commonService.user_id,"new_tags":tagArr};
								docs.edit_doc_tag(req).then(function(objS){
									if(objS.data.responseCode == 200){
										$modalInstance.close();
									}
								},function(objE){
									console.log('edit_doc_tag objE => '+JSON.stringify(objE));
								});
							}
						}
					}
				}
			}
        });
	}

/******Function to open modal for date edit******/
	sc.openDateModal = function(status, date){
		$('#date-modal').modal({backdrop: 'static'});
		if(status){
			$('#docDate').datepicker({});
		}else{
			$("#docDate").datepicker('setDate', new Date(date));
		}
	}

/******Close modal after selecting the document viewer date******/
	sc.closeDateModal = function(type){
		if(type == 'save'){
			if(isNaN($("#docDate").datepicker('getDate').getTime())){
				alertify.error('Please select date');
			}else if(sc.tabDetail._source.date == $("#docDate").datepicker('getDate').getTime()){
				alertify.error('Nothing to update');
			}else{
				sc.tabDetail._source.date = isNaN($("#docDate").datepicker('getDate').getTime())?undefined:$("#docDate").datepicker('getDate').getTime();
				var req={"document_id":sc.tabDetail._source.id,"new_date":$("#docDate").datepicker('getDate').getTime(),"user_id":commonService.user_id};
				detailAPI('edit_date', req);
				$('#date-modal').modal('hide');
			}
		}else{
			$('#date-modal').modal('hide');
		}
	}
	
/******Open modal to display the cases along with search******/
	sc.caseView = function(type){
		if(type == 'modal'){
			$('#goto-viewer-modal').modal({backdrop: 'static'});
			sc.caseSearch='';
			sc.searchCaseList=[];
		}else{
			sc.$emit('openCaseOfDoc', {id:sc.tabDetail._source.parent_case, title:'Case Viewer Title'});
		}
	}

/******API to get cases regarding keywords searched******/
	sc.searchCase = function(){
		docs.get_cases({title:sc.caseSearch}).then(function(objS){
			if(objS.data.responseCode == 200){
				sc.searchCaseList=objS.data.data;
			}
		},function(objE){
			console.log('get_cases error => '+JSON.stringify(objE));
		})
	}

/******Close case modal after selecting the cases******/
	sc.closeCaseModal = function(type, caseDetail){
		if(type == 'close')
			$('#goto-viewer-modal').modal('hide');
		else{
			var req={"user_id":commonService.user_id, "document_id":sc.docID, "case_id":caseDetail._id};
			docs.add_case_doc(req).then(function(objS){
				if(objS.data.responseCode == 200){
					$('#goto-viewer-modal').modal('hide');
				}
			},function(objE){
				console.log('get_cases error => '+JSON.stringify(objE));
			})
		}
	}

/******Citation copy functionality******/
	sc.onClipSucc = function(e) {
		e.clearSelection();
		setTooltip('Copied!');
  		hideTooltip();
	};

	sc.onClipError = function(e) {
		setTooltip('Failed!');
  		hideTooltip();
	}

	$('.clip-btn').tooltip({
		trigger: 'click',
		placement: 'bottom'
	});

	function setTooltip(message) {
		$('.clip-btn').tooltip('hide')
		.attr('data-original-title', message)
		.tooltip('show');
	}

	function hideTooltip() {
		setTimeout(function() {
			$('.clip-btn').tooltip('hide');
		}, 1000);
	}

/******Scroll to particular paragraph******/
	sc.$on('goToPara', function(event, args) {
		$timeout(function() {
			var newHash = 'anchor'+args.paraID;
			if ($location.hash() !== newHash) {
				$location.hash(newHash);
			} else {
				$anchorScroll();
			}
		}, 500);
	})
	
}]);






// $location.hash('docViewTop');
// $anchorScroll();
// sc.tabArr[sc.activeTabIndex].name='Loading...';