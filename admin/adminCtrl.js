'use strict';

angular.module('Lawcunae.admin', [])

.controller('adminCtrl', ['$scope', 'docs', 'commonService', '$sce', '$timeout', '$rootScope', 'API_URL', 'storage', 'user' ,function(sc, docs, commonService, $sce, $timeout, $rootScope, API_URL, storage, user) {

/******Court name select2 implementation******/
    $(".court_admin").select2({
        placeholder: "Filter for court name",
        ajax: {
            url: API_URL+"documentActivity/court_auto_suggest",
            dataType: 'json',
            beforeSend: function (xhr) { 
                xhr.setRequestHeader("Authorization", storage.get('token'));
            },
            delay: 250,
            data: function (params) {
                return {
                    suggestion: params.term
                };
            },
            processResults: function (data, params) {
            return {
                    results: data.data
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 1,
        templateResult: formatRepoCourt,
        templateSelection: formatRepoSelectionCourt
    }).trigger('change');

    sc.tabType='indian_kanoon_new';
    sc.filter={search:''};
    $('.selectDate').datepicker({});
    getDocsList(1);

    

    function formatRepoCourt (repo) {
		if (repo.loading) return "Loading terms..";
			return "<div>"+repo.court_name+"</div>";
	}

	function formatRepoSelectionCourt (repo) {
		return repo.court_name;
	}

/******API to get documents list functionality******/
    function getDocsList(pageIndex){
        var filterVal={};
        var courtArr=[];
		for(var i=0;i<$(".court_admin").select2('data').length;i++){
			courtArr.push($(".court_admin").select2('data')[i].court_name);
		}
        isNaN($("#fromfilterDate").datepicker('getDate').getTime()) == true ? '' : (filterVal.from = $("#fromfilterDate").datepicker('getDate').getTime());
        isNaN($("#tofilterDate").datepicker('getDate').getTime()) == true ? '' : (filterVal.to = $("#tofilterDate").datepicker('getDate').getTime());
        courtArr.length==0?'':(filterVal.court = courtArr);
        var searchReq={
			search:sc.filter.search,
            filter:filterVal,
            type:sc.tabType
        };

        if(sc.tabType == 'indian_kanoon_new' || sc.tabType == 'lawcunae_case' || sc.tabType == 'lawcunae_article'){
            docs.gel_admin_docs(searchReq, pageIndex).then(function(objS){
                if(objS.data.responseCode == 200){
                    sc.adminDocList=objS.data.data;
                    var page = (objS.data.total)%100;
                    page == 0 ? page=Math.floor((objS.data.total)/100) : page=Math.floor((objS.data.total)/100)+1;
                    sc.totalPage=new Array(page);
                    sc.activeSearchPage=pageIndex;
                    sc.pageLimitIndex=1;
                }
            },function(objE){
                console.log('upload_doc error => '+JSON.stringify(objE));
            })
        }else{
            if(sc.tabType == 'lawcunae_user')
                var end_point='users_list';
            else
                var end_point='user_payment_history';
            docs.user_list(searchReq, pageIndex, end_point).then(function(objS){
                if(objS.data.responseCode == 200){
                    sc.adminDocList=objS.data.data.docs;
                    var page = (objS.data.data.total)%100;
                    page == 0 ? page=Math.floor((objS.data.data.total)/100) : page=Math.floor((objS.data.data.total)/100)+1;
                    sc.totalPage=new Array(page);
                    sc.activeSearchPage=pageIndex;
                    sc.pageLimitIndex=1;
                }
            },function(objE){
                console.log('upload_doc error => '+JSON.stringify(objE));
            })
        }
    }

/******Change pages functionality******/
    sc.changePageArrow = function(type){
		if(type == 'prev'){
			if(sc.pageLimitIndex > 1)
				sc.pageLimitIndex = sc.pageLimitIndex-10;
		}else{
			if(sc.totalPage.length > (sc.pageLimitIndex+10))
				sc.pageLimitIndex = sc.pageLimitIndex+10;
		}
    }
    
/******Pagination functionality******/
    sc.changePage = function(pageNumber){
		if(sc.activeSearchPage != pageNumber){
			sc.activeSearchPage=pageNumber;
            sc.adminDocList=[];
            getDocsList(sc.activeSearchPage);
		}
    }

/******Tab change functionality******/
    sc.tabClick = function(type){
        sc.filter={search:''};
        $("#fromfilterDate").val('');
        $("#tofilterDate").val('');
        sc.adminDocList=[];
        sc.tabType=type;
        sc.totalPage=[];
        sc.activeSearchPage=1;
        sc.pageLimitIndex=1;
        getDocsList(1);
    }

/******Filter functionality on lists******/
    sc.filterList = function(){
        getDocsList(1);
    }

/******Verify particular document, article viewer, case viewer functionality******/
    sc.verifyDocs = function(doc, type, param){
        var end_point=type+'?'+param+'='+doc._id+'&isVerified='+doc._source.isVerified;
        docs.verify_docs(end_point).then(function(objS){
            if(objS.data.responseCode == 200){
                
            }
        },function(objE){
            console.log('upload_doc error => '+JSON.stringify(objE));
        })
    }

/******Delete particular document, article viewer, case viewer functionality******/
    sc.deleteDoc = function(index, doc, type, param){
        var end_point=type+'?'+param+'='+doc.user_id;
        docs.delete_docs(end_point).then(function(objS){
            if(objS.data.responseCode == 200){
                sc.adminDocList.splice(index, 1);
            }
        },function(objE){
            console.log('upload_doc error => '+JSON.stringify(objE));
        })
    }

/******Particlar documents click functionality******/
	sc.documentClk = function(data, type){
        if(type == 'doc')
            sc.$emit('docDetailEmit', {link:data._source.id, source:'annotation', text:data._source.title});
        else if(type == 'case')
            sc.$emit('docDetailEmit', {link:data._source.case_id, source:'case', text:data._source.title});
        else if(type == 'article')
            sc.$emit('docDetailEmit', {link:data._source.article_id, source:'article', text:data._source.title});
        else if(type == 'user')
            sc.$emit('docDetailEmit', {link:data.user_id, source:'profile', text:data.profile.first_name});
    }
    
/******Make users as admin functionality******/
    sc.makeAdmin = function(data, status){
        user.make_admin(data.user_id, status).then(function(objS){
            if(objS.data.responseCode == 200){
                data.profile.isAdmin=status;
            }
        },function(objE){
            console.log('upload_doc error => '+JSON.stringify(objE));
        })
    }

}]);