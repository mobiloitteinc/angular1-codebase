'use strict';

angular.module('Lawcunae.landingPage', [])

.controller('landingPageCtrl', ['$scope', 'user', 'storage', '$timeout', 'commonService', '$state', '$rootScope', '$location',function(sc, user, storage, $timeout, commonService, $state, $rootScope, $location) {
	
	sc.topTabClk = function(page){
		$state.go('landing_page.'+page);
	}
	
}]);