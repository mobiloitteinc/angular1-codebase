'use strict';

angular.module('Lawcunae.draft_list', [])

.controller('draftListCtrl', ['$rootScope', '$scope', '$state', 'docs', 'commonService',function($rootScope, sc, $state, docs, commonService) {

    sc.user_info = commonService.user_info;
    if(sc.user_info != null){
        sc.draftList = sc.user_info.drafts;
    }

/***********Navigation to add draft page for new draft*************/
    sc.addDraft = function(){
        commonService.draftType='new';
        sc.$emit('menuOpen', {name:'draftEditorTab', index:-6, page:'draft_editor'});
    }

/***********Navigation to add draft page along with details*************/
    sc.draftClk = function(draft){
        commonService.draftType=draft.draft_id;
        sc.$emit('menuOpen', {name:'draftEditorTab', index:-6, page:'draft_editor'});
    }
    
}]);