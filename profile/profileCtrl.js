'use strict';

angular.module('Lawcunae.profile', [])

.controller('profileCtrl', ['$scope', 'commonService', 'docs', 'user', '$rootScope', '$modal', '$timeout', 'storage', 'API_URL', '$sce', '$http', 'alertify',function(sc, commonService, docs, user, $rootScope, $modal, $timeout, storage, API_URL, $sce, $http, alertify) {
	var emailRegx = /^[A-Z0-9_]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,3})+$/i;
	var mobileRegx = /^[789]\d{9}$/;
	var nameRegx = /^[^'][a-zA-Z' ]*[^']$/;
	sc.editBtnStatus=false;
	var timelineTempArr=[];
	sc.popup={};
	var timeline = [
        {
          "type": "internship",
          "date": 1511239239000,
          "to": "00-00-0000",
          "organization": {
            "designation": "Contract Engineer",
            "department": "Microwaves",
            "name": "DRDO, Hyderabad",
            "website": "www.dlrl.drdo.gov.in"
          }
		},
		{
          "type": "education",
          "date": 1479703239000,
          "to": "00-00-0000",
          "organization": {
            "course": "B.Tech in computer science and engineering",
            "department": "CSE",
            "name": "IIIT Kota",
            "website": "www.iiitkota.ac.in"
          }
        },
		{
          "type": "case",
		  "date":1479703239000,
          "name": "IT returns fraud by Satyam"
        },
        {
          "type": "case",
		  "date":1479703239000,
          "name": "Software Patenting"
		},
		{
          "type": "publication",
		  "date":1479703239000,
          "publisher": "IEEE Access",
          "name": "Applying IPR Laws in practise"
		},
		{
			"type": "article",
			"date": 1479703239000,
			"name": "Software Article",
			"publisher": "Digit India",
			"url": "http://digitindia.com"
		}
	  ];

	sc.profileID = commonService.stateChangeVal.value;
	if(commonService.user_info!=null){
		sc.user_id = commonService.user_info.user_id;
	}

	if(sc.profileID!= undefined && (sc.profileID != commonService.user_id)){
		if(commonService.stateChangeVal.exist){
			storeExistingList();
		}else{
			getUserProfile(sc.profileID);
		}
	}

/******Get detail of existing user store locally******/
	function storeExistingList(){
		sc.user_info={};
		var index=commonService.allTabsContent.profile.findIndex((x) => x.keyword == commonService.stateChangeVal.value);
		if(index != -1){
			if(commonService.allTabsContent.profile[index] != undefined){
				sc.user_info=commonService.allTabsContent.profile[index].user_info;
				$timeout(function(){
					casesDocs();
				},500);	
			}
		}else{
			getUserProfile(sc.profileID);
		}
	}

/******API to fetch user profiles detail******/
	function getUserProfile(profile_id){
		user.user_info(parseInt(profile_id)).then(function(objS){
			if(objS.data.responseCode == 200){
				sc.user_info=objS.data.data;
				if(objS.data.data != null){
					commonService.allTabsContent.profile.push({user_info:sc.user_info, keyword:profile_id});
					$timeout(function(){
						casesDocs();
					},500);
				}
			}
		},function(objE){

		})
	}

/******Open same tab when came from different profile viewer******/
	sc.$on('sameTypeTab', function (event, args) {
		sc.profileID = args.id.value;
		$rootScope.userupdated++;
		if(sc.profileID!= undefined && (sc.profileID != commonService.user_id)){
			if(commonService.stateChangeVal.exist){
				storeExistingList();
			}else
				getUserProfile(args.id.value);
		}
	});

/******Invoke whenever user profile updated*******/
	$rootScope.$watch('userupdated',function(){
		if(sc.profileID == commonService.user_id){
			sc.user_info = commonService.user_info;
			$timeout(function(){
				casesDocs();
			},500);
		}
	});
	
/*****code to modify the document cases as like to display in UI*******/
	function casesDocs(){
		// sc.user_info.profile.relavant_case_documents=[2];
		// sc.user_info.profile.timeline=timeline;
		sc.user_info.profile.timelineArr=[];
		timelineTempArr=[];
		timelineFunc();
		sc.user_info.profile.case_documents=[];
		recursionDocs(0, sc.user_info.profile.relavant_case_documents);
	}

/*****code to modify the timeline as like to display in UI*******/
	function timelineFunc(){
		for(var i=0;i<sc.user_info.profile.timeline.length;i++){
			var index=timelineTempArr.findIndex((x) => x.year == new Date(sc.user_info.profile.timeline[i].date).getFullYear());
			if(index==-1){
				timelineTempArr.push({year:new Date(sc.user_info.profile.timeline[i].date).getFullYear(), timelineList:[{type:sc.user_info.profile.timeline[i].type, list:[sc.user_info.profile.timeline[i]]}]});
			}else{

				var typeInd=timelineTempArr[index].timelineList.findIndex((y) => y.type == sc.user_info.profile.timeline[i].type);
				if(typeInd==-1){
					timelineTempArr[index].timelineList.push({type:sc.user_info.profile.timeline[i].type, list:[sc.user_info.profile.timeline[i]]});
				}else{
					timelineTempArr[index].timelineList[typeInd].list.push(sc.user_info.profile.timeline[i]);
				}
			}
			if(i == sc.user_info.profile.timeline.length-1){
				sc.user_info.profile.timelineArr=angular.copy(timelineTempArr);
				// console.log('timelineArr => '+JSON.stringify(sc.user_info.profile.timelineArr));
			}
		}
	}

/*****Recursive function to fetch the document detail of each relavant_case_documents*******/
	function recursionDocs(counter, item){
		if(counter < item.length){
			docs.get_paragraphs({"document_id":item[counter].id}).then(function(objS){
				if(objS.data.responseCode == 200){
					objS.data.data[0].rel_case_id=item[counter]._id;
					sc.user_info.profile.case_documents.push(objS.data.data[0]);
					counter++;
					return recursionDocs(counter, sc.user_info.profile.relavant_case_documents);
				}
			},function(objE){
				console.log('paragraph error => '+JSON.stringify(objE));
			})
		}else{
			// console.log('recursion complete');
			// console.log('user_info => '+JSON.stringify(sc.user_info));
		}
	}

	$('.selectDate').datepicker({

	});

/********Checking whether the particular statement is url or not*******/
	function isUrlValid(userInput) {
		var res = userInput.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
		if(res == null)
			return false;
		else
			return true;
	}

/******Javascript file loading function*******/
	function async(u, c) {
		var d = document, t = 'script',
			o = d.createElement(t),
			s = d.getElementsByTagName(t)[0];
		o.src = '//' + u;
		if (c) { o.addEventListener('load', function (e) { c(null, e); }, false); }
		s.parentNode.insertBefore(o, s);
	}

	async('cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js', function() {
		$('.js-example-basic-multiple').select2();
	});

/*****Functionality of edit profile*******/
	sc.editUserProfile = function(){
		if(sc.profileID!= undefined && (sc.profileID != commonService.user_id))
			return;
		if(!sc.editBtnStatus){
			sc.editBtnStatus=!sc.editBtnStatus;
			openLocationSelect();
			var fname = sc.user_info.profile.first_name == undefined ? '' : sc.user_info.profile.first_name;
			var mname = sc.user_info.profile.middle_name == undefined ? '' : sc.user_info.profile.middle_name;
			var lname = sc.user_info.profile.last_name == undefined ? '' : sc.user_info.profile.last_name;
			sc.popup = {
				image: sc.user_info.profile.image,
				prefix: sc.user_info.profile.prefix == undefined ? '' : sc.user_info.profile.prefix,
				first_name:sc.user_info.profile.first_name == undefined ? '' : sc.user_info.profile.first_name,
				middle_name:sc.user_info.profile.middle_name == undefined ? '' : sc.user_info.profile.middle_name,
				last_name:sc.user_info.profile.last_name == undefined ? '' : sc.user_info.profile.last_name,
				name: fname+' '+mname+' '+lname,
				contact : {
					mobile: sc.user_info.profile.contact == undefined ? '' : sc.user_info.profile.contact.mobile,
					work: sc.user_info.profile.contact == undefined ? '' : sc.user_info.profile.contact.work
				},
				profession: sc.user_info.profile.profile_header == undefined ? '' : sc.user_info.profile.profile_header,
				bio: sc.user_info.profile.bio == undefined ? '' : sc.user_info.profile.bio	  
			};
		}else{
			if (!navigator.onLine) {
				alertify.error('Please check your network connection')
			} else if (sc.popup.first_name == '') {
				alertify.error('Please enter first name')
			} else if (!nameRegx.test(sc.popup.first_name)) {
				alertify.error('Please enter valid first name')
			} else if (sc.popup.name == '') {
				alertify.error('Please enter full name')
			} else if (!nameRegx.test(sc.popup.name)) {
				alertify.error('Please enter valid name')
			} else if (sc.popup.contact.mobile == '') {
				alertify.error('Please enter primary contact number')
			} else if (!mobileRegx.test(sc.popup.contact.mobile)) {
				alertify.error('Please enter valid contact number')
			} else if ((sc.popup.contact.work) != '' && (!mobileRegx.test(sc.popup.contact.work))) {
				alertify.error('Please enter valid contact number')
			} else if ($(".location_select").select2('data')[0] == undefined || $(".location_select").select2('data')[0].description == undefined) {
				alertify.error('Please select location')
			} else if (sc.popup.profession == '') {
				alertify.error('Please enter profession')
			} else if (sc.popup.bio == '') {
				alertify.error('Please enter bio')
			} else {
				if(sc.popup.name.indexOf(" ")==-1) {
					var firstName = sc.popup.name;
					var middleName = '';
					var lastName = '';
				} else if(sc.popup.name.split(" ").length==2) {
					var firstName = sc.popup.name.split(' ')[0];
					var middleName = '';
					var lastName = sc.popup.name.split(' ')[1];
				} else if(sc.popup.name.split(" ").length>=3) {
					var firstName = sc.popup.name.split(' ')[0];
					var middleName = sc.popup.name.split(' ')[1];
					var lastName = sc.popup.name.split(' ')[2];
				}
				
				var edit_req = {
					"user_id": commonService.user_id,
					"prefix": sc.popup.prefix,
					"first_name": sc.popup.first_name == undefined ? firstName : sc.popup.first_name,
					"middle_name": sc.popup.middle_name == undefined ? middleName : sc.popup.middle_name,
					"last_name": sc.popup.last_name == undefined ? lastName : sc.popup.last_name,
					"contact": sc.popup.contact,
					"location": $(".location_select").select2('data')[0].description,
					"bio": sc.popup.bio,
					"profile_header": sc.popup.profession,
					"image": sc.popup.image
				};

				if(!isUrlValid(sc.popup.image)){
					user.upload_userImage({	"image":sc.popup.image.substr(22)}).then(function(objS){
						if(objS.data.responseCode == 200){
							edit_req.image = objS.data.url;
							updateInfoApi(edit_req);
						}else
							updateInfoApi(edit_req);
					},function(objE){
						updateInfoApi(edit_req);
						console.log('paragraph error => '+JSON.stringify(objE));
					})
				}else{
					updateInfoApi(edit_req);
				}
			}
		}
	}

/****API to update the user info*****/
	function updateInfoApi(edit_req){
		user.edit_user_info(edit_req).then(function(objS){
			sc.editBtnStatus=!sc.editBtnStatus;
			if(objS.data.responseCode == 200){
				commonService.user_info=objS.data.data;
				alertify.success(objS.data.responseMessage)
				// $rootScope.userupdated++;
				$rootScope.userupdated=$rootScope.userupdated+1;
			}else
				alertify.error(objS.data.responseMessage)
		},function(objE){
			console.log('paragraph error => '+JSON.stringify(objE));
		})
	}

	sc.closePopover = function(type){
		if(type == 'profile')
			sc.editBtnStatus=!sc.editBtnStatus;
		sc.editAboutPopupStatus=undefined;
	}

/****Edit about tab section functionality******/
	sc.editAbout = function(type){
		if(sc.profileID!= undefined && (sc.profileID != commonService.user_id))
			return;
		sc.editAboutPopupStatus=type;
		if(type == 'about'){
			if(!CKEDITOR.instances['aboutCKEditor'])
				CKEDITOR.replace( 'aboutCKEditor', {
					removeButtons: 'Source',
				} );
		}else if(type == 'EandB'){
			sc.popup={heading:'add', educationType:'education', fromDate:'', toDate:'', institute:'', course:'', department:'', studyfield:'', location:'', logo:'', workProfile:''};
			$("#fromEDDate").val('');
			$("#toEDDate").val('');
			openLocationSelect();
		}else if(type == 'interest'){
			$timeout(function(){
				openInterestSelect();
			},10);
		}
	}

/******Select2 initialization for location selector******/
	function openLocationSelect(){
		$(".location_tag, .location_select").select2({
			placeholder: "Search for Location",
			ajax: {
				// url: "http://172.16.6.242:8888/documentActivity/google_places",
				url: API_URL+"documentActivity/google_places",
				dataType: 'json',
				beforeSend: function (xhr) { 
					xhr.setRequestHeader("Authorization", storage.get('token'));
				},
				delay: 250,
				data: function (params) {
					return {
						place: params.term
					};
				},
				processResults: function (data, params) {
				return {
						results: data.data
					};
				},
				cache: true
			},
			escapeMarkup: function (markup) { return markup; },
			minimumInputLength: 1,
			templateResult: formatRepoLoc,
			templateSelection: formatRepoSelectionLoc
		}).trigger('change');

	}

	function formatRepoLoc (repo) {
		if (repo.loading) return "Loading location..";
		    return "<div>"+repo.description+"</div>";
	}

	function formatRepoSelectionLoc (repo) {
		return repo.description;
	}

/******Edit functionality for education and background******/
	sc.editEandB = function(type, background){
		if(sc.profileID!= undefined && (sc.profileID != commonService.user_id))
			return;
		sc.editAboutPopupStatus=type;
		// console.log('background => '+JSON.stringify(background));
		$timeout(function(){
			$("#fromEDDate").datepicker('setDate', new Date(background.from));
			$("#toEDDate").datepicker('setDate', new Date(background.to));
		},500);
		if(type == 'EandB'){
			if(background.type == 'education'){
				sc.popup={_id:background._id, heading:'Edit', educationType:background.type, fromDate:'', toDate:'', institute:background.organization.name, course:background.organization.course, department:background.organization.department, studyfield:background.organization.fieldStudy, location:'', logo:'', workProfile:''};
				(background.organization.logo!=undefined && background.organization.logo!='')?sc.popup.logo=background.organization.logo:'';
			}else if(background.type == 'work'){
				sc.popup={_id:background._id, heading:'Edit', educationType:background.type, fromDate:'', toDate:'', institute:background.organization.name, course:'', department:'', studyfield:'', location:'', logo:'', workProfile:background.description};
				(background.organization.logo!=undefined && background.organization.logo!='')?sc.popup.logo=background.organization.logo:'';
			}
		}
	}

/******Submit about tab functionality********/
	sc.submitAbout = function(type){
		// sc.editAboutPopupStatus=undefined;
		if(type == 'about'){
			var len = (CKEDITOR.instances['aboutCKEditor'].getData().replace(/<\/?[^>]+(>|$)/g, "")).length;
			if (len - 1 < 20) {
				alertify.error('About us should contain atleast 20 characters')
			} else {
				var req={"user_id":commonService.user_id,"about":CKEDITOR.instances['aboutCKEditor'].getData()};
				sc.editAboutPopupStatus=undefined;
				editAboutFunc(req, 'update_user_about');
			}
		}else if(type == 'interest'){
			var interestArr=[];
			for(var i=0;i<$(".interest_tag").select2('data').length;i++){
				interestArr.push($(".interest_tag").select2('data')[i].tag);
			}
			var req={"user_id":commonService.user_id,"interests":interestArr};
			sc.editAboutPopupStatus=undefined;
			editAboutFunc(req, 'update_user_interests');
		}else if(type == 'EandB'){
			if (!navigator.onLine) {
				alertify.error('Please check your network connection')
			} else if (sc.popup.institute == '') {
				alertify.error('Please enter institute name')
			} else if (sc.popup.department == '' && sc.popup.educationType == 'education') {
				alertify.error('Please enter department')
			} else if ($(".location_tag").select2('data')[0] == undefined || $(".location_tag").select2('data')[0].description == undefined) {
				alertify.error('Please select location')
			} else {
				var fromDate = isNaN($("#fromEDDate").datepicker('getDate').getTime()) == true ? '' : $("#fromEDDate").datepicker('getDate');
				var toDate = isNaN($("#toEDDate").datepicker('getDate').getTime()) == true ? '' : $("#toEDDate").datepicker('getDate');
				if (fromDate == '') {
					alertify.error('Please enter from date')
				} else if ((fromDate).getTime() < new Date().getTime()) {
					alertify.error('From date should be greater than current date')
				} else if ((toDate == '') || ((toDate).getTime() < (fromDate).getTime())) {
					alertify.error('to date should be greater than from date')
				} else if(sc.popup.logo != '' && !isUrlValid(sc.popup.logo)){
						user.upload_userImage({	"image":sc.popup.logo.substr(22)}).then(function(objS){
							if(objS.data.responseCode == 200){
								sc.popup.logo = objS.data.url;
								updateEBApi();
							}else
								updateEBApi();
							sc.editAboutPopupStatus = undefined;
						},function(objE){
							updateEBApi();
							sc.editAboutPopupStatus = undefined;
							console.log('paragraph error => '+JSON.stringify(objE));
						})
					}else{
						updateEBApi();
						sc.editAboutPopupStatus = undefined;
					}
			}
		}
	}

/******Manage data for eductaion and background******/
	function updateEBApi(){
		if(sc.popup.educationType == 'education'){
				var req={"user_id":commonService.user_id,
						"background":   {
							"type": sc.popup.educationType,
							"from": $("#fromEDDate").datepicker("getDate").getTime(),
							"to": $("#toEDDate").datepicker("getDate").getTime(),
							"organization": {
								"course": sc.popup.course,
								"department": sc.popup.department,
								"name": sc.popup.institute,
								"location": $(".location_tag").select2('data')[0].description,
								"fieldStudy":sc.popup.studyfield,
								"logo":sc.popup.logo
							}
						}
					}
				if(sc.popup.heading == 'Edit'){
					req.background_id=sc.popup._id;
					var endPoint='edit_user_background';
				}else
					var endPoint='add_background';
				editAboutFunc(req, endPoint);
			}else if(sc.popup.educationType == 'work'){
				var req={"user_id":commonService.user_id,
						"background":   {
							"type": sc.popup.educationType,
							"from": $("#fromEDDate").datepicker("getDate").getTime(),
							"to": $("#toEDDate").datepicker("getDate").getTime(),
							"organization": {
								"name": sc.popup.institute,
								"location": $(".location_tag").select2('data')[0].description,
								"logo":sc.popup.logo
							},
							"description": sc.popup.workProfile
						}
					}
				if(sc.popup.heading == 'Edit'){
					req.background_id=sc.popup._id;
					var endPoint='edit_user_background';
				}else
					var endPoint='add_background';
				editAboutFunc(req, endPoint);
			}
	}

/*****Edit about API integration*****/
	function editAboutFunc(req, end_point){
		user.update_about(req, end_point).then(function(objS){
			if(objS.data.responseCode == 200){
				commonService.user_info=objS.data.data;
				// $rootScope.userupdated++;
				$rootScope.userupdated=$rootScope.userupdated+1;
			}
		},function(objE){
			console.log('paragraph error => '+JSON.stringify(objE));
		})
	}

	sc.uploadLogo = function(input){
		if (input.files && input.files[0]) {
			var reader = new FileReader(input);
			reader.onload = function (e) {
				sc.popup.logo=this.result;
			}
			reader.readAsDataURL(input.files[0]);
		}
	}

/******Upload picture functionality******/
	sc.uploadPicture = function (input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader(input);
			reader.onload = function (e) {
				var tempPic=this.result;
				sc.$apply();
				var modalInstance = $modal.open({
					template: '<div class="cropArea"><img-crop image="myImage" result-image="myCroppedImage"></img-crop></div><div>Cropped Image:</div><div><img ng-src="{{myCroppedImage}}" /></div><button class="btn btn-primary custom-black" ng-click="cropImage()">Crop</button>',
					controller:function($scope, $modalInstance, $timeout){
						$scope.myImage=tempPic;
						$scope.myCroppedImage='';

						$scope.cropImage = function(){
							sc.popup.image = $scope.myCroppedImage;
							$modalInstance.close();
						}
					}
				});
				modalInstance.result.then(function (success) {
					
				}, function () {
					console.log('Modal dismissed => ');
				});
			}
			reader.readAsDataURL(input.files[0]);
		}
	}

/******Interest select2 dropdown******/
	function openInterestSelect(){
		$(".interest_tag").select2({
			placeholder: "Search for interests",
			containerCssClass : "interest_popup_span",
			ajax: {
				url: API_URL+"documentActivity/auto_suggest",
				dataType: 'json',
				beforeSend: function (xhr) { 
					xhr.setRequestHeader("Authorization", storage.get('token'));
				},
				delay: 250,
				data: function (params) {
					return {
						suggestion: params.term
					};
				},
				processResults: function (data, params) {
				return {
						results: data.data
					};
				},
				cache: true
			},
			escapeMarkup: function (markup) { return markup; },
			minimumInputLength: 1,
			templateResult: formatRepoDoc,
			templateSelection: formatRepoSelectionDoc
		}).trigger('change');

		$(".interest_tag1").select2({
			data: [{
				id: 0,
				text: '2015'
			}, {
				id: 1,
				text: '2014'
			}],
			val: ["0"]
		}).select2('val', [1]);
	}

	function formatRepoDoc (repo) {
		if (repo.loading) return "Loading terms..";
		    return "<div>"+repo.tag+"</div>";
	}

	function formatRepoSelectionDoc (repo) {
		return repo.tag;
	}

	sc.trustAsHtml = function(html) {
		return $sce.trustAsHtml(html);
	}

	sc.searchCaseDoc = function(){
		docs.search_case_doc({title:sc.docCaseModal.search}).then(function(objS){
			if(objS.data.responseCode == 200){
				sc.searchCaseDocList=objS.data.data;
			}
		},function(objE){
			console.log('paragraph error => '+JSON.stringify(objE));
		})
	}

	sc.datePickerOpen = function(type){
		if(type == 'from')
			$("#addTimeFrom").datepicker("show");
		else
			$("#addTimeTo").datepicker("show");
	}

/******Open popup to add timeline******/
	sc.addTimeline = function(){
		if(sc.profileID!= undefined && (sc.profileID != commonService.user_id))
			return;
		sc.modal={heading:'Add', type:"internship", designation:"", course:"", department:"", name:"", publisher:"", website:""};
		$("#addTimeFrom").val('');
		$("#addTimeTo").val('');
		$('#timeline-event-modal').modal({backdrop: 'static'});	
	}

/******Edit timeline functionality******/
	sc.editTimelineDetail = function(status, list) {
		if(sc.profileID!= undefined && (sc.profileID != commonService.user_id))
			return;
		$('#timeline-event-modal').modal({backdrop: 'static'});
		$timeout(function(){
			$("#addTimeFrom").datepicker('setDate', new Date(list.date));
			if(list.type == 'internship' || list.type == 'education')
				$("#addTimeTo").datepicker('setDate', new Date(list.to));
		},500);
		switch(list.type){
			case 'internship':
				sc.modal={_id:list._id, heading:'Edit', type:list.type, designation:list.organization.designation, course:"", department:list.organization.department, name:list.organization.name, publisher:"", website:list.organization.website};
				break;
			case 'education':
				sc.modal={_id:list._id, heading:'Edit', type:list.type, designation:"", course:list.organization.course, department:list.organization.department, name:list.organization.name, publisher:"", website:list.organization.website};
				break;
			case 'publication':
				sc.modal={_id:list._id, heading:'Edit', type:list.type, designation:"", course:"", department:"", name:list.name, publisher:list.publisher, link:""};
				break;
			case 'case':
				sc.modal={_id:list._id, heading:'Edit', type:list.type, designation:"", course:"", department:"", name:list.name, publisher:"", link:""};
				break;
			case 'article':
				sc.modal={_id:list._id, heading:'Edit', type:list.type, designation:"", course:"", department:"", name:list.name, publisher:list.publisher, link:list.website};
				break;
		}
	};

/******Function to submit timeline for own profile******/
	sc.submitTimeline = function(openStatus){
		if(openStatus){
			var req={
					"user_id":commonService.user_id,
					"timeline":
						{
							"type": sc.modal.type,
							"date": $("#addTimeFrom").datepicker("getDate").getTime(),
						}
					}
			if(sc.modal.heading == 'Edit'){
				req.timeline_id=sc.modal._id;
				var endPoint='edit_user_timeline';
			}else
				var endPoint='add_timeline';

			switch (sc.modal.type) {
				case 'internship':
					req.timeline.to=$("#addTimeTo").datepicker("getDate").getTime();
					req.timeline.organization={designation:sc.modal.designation, department:sc.modal.department, name:sc.modal.name, website:sc.modal.website};
					break;
				case 'education':
					req.timeline.to=$("#addTimeTo").datepicker("getDate").getTime();
					req.timeline.organization={course:sc.modal.course, department:sc.modal.department, name:sc.modal.name, website:sc.modal.website};
					break;
				case 'publication':
					req.timeline.name=sc.modal.name;
					req.timeline.publisher=sc.modal.publisher;
					break;
				case 'case':
					req.timeline.name=sc.modal.name;
					break;
				case 'article':
					req.timeline.name=sc.modal.name;
					req.timeline.publisher=sc.modal.publisher;
					req.timeline.website=sc.modal.website;
					break;
			}
			// console.log('req => '+JSON.stringify(req));
			if (!navigator.onLine) {
				alertify.error('Please check your network connection')
			} else if (req.timeline.name == '') {
				alertify.error('Please enter name')
			} else if (req.timeline.type == 'internship' && req.timeline.organization.designation == '') {
				alertify.error('Please enter designation')
			} else if (req.timeline.type == 'education' && req.timeline.organization.course == '') {
				alertify.error('Please enter course')
			} else if ((req.timeline.type == 'internship' || req.timeline.type == 'education') && req.timeline.organization.department == '') {
				alertify.error('Please enter department')
			} else if ((req.timeline.type == 'publication' || req.timeline.type == 'article') && req.timeline.publisher == '') {
				alertify.error('Please enter publisher')
			} else if ((req.timeline.type != 'case' && req.timeline.type != 'publication') && req.timeline.website == '') {
				alertify.error('Please enter website link')
			} else if (isNaN(req.timeline.date) || req.timeline.date == '') {
				alertify.error('Please enter from date')
			}else if(req.timeline.date < new Date().getTime()){
				alertify.error('From date should be greater than current date')
			}else if((req.timeline.type == 'internship' || req.timeline.type == 'education') && req.timeline.to < req.timeline.date){
				alertify.error('to date should be greater than from date')
			} else {
				user.action_timeline(req, endPoint).then(function(objS){
					if(objS.data.responseCode == 200){
						commonService.user_info=objS.data.data;
						sc.user_info.profile.timeline=commonService.user_info.profile.timeline;
						timelineTempArr=[];
						timelineFunc();
						$('#timeline-event-modal').modal('hide');
					}
				},function(objE){
					console.log('paragraph error => '+JSON.stringify(objE));
				})
			}
		}else
			$('#timeline-event-modal').modal('hide');
	}

/******Add article functionality******/
	sc.addArticle = function(status){
		if(sc.profileID!= undefined && (sc.profileID != commonService.user_id))
			return;
		if(status == 'addArticle'){
			$('#newArticleModal').modal({backdrop: 'static'});
			sc.searchCaseDocList=[];
			sc.docCaseModal={search:''};
		}else if(status == 'addCase'){
			$('#newArticleModal').modal('hide');
			$('#newCaseModal').modal({backdrop: 'static'});
			sc.case={title:'', summary:'', search:''};
			sc.tempDocList=[];
			sc.searchDocList=[];
		}else{
			$('#newArticleModal').modal('hide');
		}
	}

/******Search document while adding cases******/
	sc.searchDoc = function(){
		docs.search_doc({title:sc.case.search}).then(function(objS){
			if(objS.data.responseCode == 200){
				sc.searchDocList=objS.data.data;
			}
		},function(objE){
			console.log('paragraph error => '+JSON.stringify(objE));
		})
	}

/******Add relevant document for cases functionality******/
	sc.addRelevantCaseDocs = function(docCase, addStatus){
		if(addStatus){
			var req={"user_id":commonService.user_id,"relavant_case_documents":{"_type":docCase._type,"id":docCase._source.id}}
			var end_point='add_relavant_case_documents';
		}else{
			var req={"user_id":commonService.user_id,"relavant_case_documents_id":docCase.rel_case_id};
			var end_point='delete_relavant_case_documents';
		}
		user.action_relavant_case_documents(req, end_point).then(function(objS){
			if(objS.data.responseCode == 200){
				commonService.user_info=objS.data.data;
				sc.user_info.profile.relavant_case_documents = commonService.user_info.profile.relavant_case_documents;
				sc.user_info.profile.case_documents=[];
				recursionDocs(0, sc.user_info.profile.relavant_case_documents);
			}
		},function(objE){
			console.log('paragraph error => '+JSON.stringify(objE));
		})
	}

/******Add relevant documents functionality******/
	sc.addRelevantDocs = function(doc, addStatus){
		if(addStatus){
			sc.tempDocList.push({id:doc._source.id, title:doc._source.title, courtName:doc._source.courtName});
		}else{
			var index=sc.tempDocList.findIndex((x) => x.id == doc.id);
			if(index!=-1){
				sc.tempDocList.splice(index,1);
			}
		}
	}

/******Add cases functionality******/
	sc.addCase = function(status){
		if(status){
			var docs=[];
			for(var i=0;i<sc.tempDocList.length;i++){
				docs.push(sc.tempDocList[i].id);
			}
			var req={"user_id":commonService.user_id, "document_list":docs, "case_title":sc.case.title, "case_description":sc.case.summary};
			if (!navigator.onLine) {
				alertify.error('Please check your network connection')
			} else if (req.case_title == '') {
				alertify.error("Please enter title")
			} else if (req.case_description == '') {
				alertify.error("Please enter summary")
			} else {
				$http({
					method: 'POST',
					url: API_URL+'documentActivity/add_case',
					data:req
				}).then(function(succ){
					if(succ.data.responseCode == 200){
						$('#newCaseModal').modal('hide');
					}
				},function(err){
					deff.reject(err);
				});
			}
		}else{
			$('#newCaseModal').modal('hide');	
		}
	}

/******Follow other user functionality******/
	sc.followUser = function(){
		var req={"user_id":commonService.user_id,"follow_id":sc.profileID};
		user.follow_user(req).then(function(objS){
			if(objS.data.responseCode == 200){
				commonService.user_info=objS.data.data;
				// sc.user_info.profile.following=commonService.user_info.profile.following;
				// sc.user_info.profile.followers=commonService.user_info.profile.followers;
			}
		},function(objE){
			console.log('paragraph error => '+JSON.stringify(objE));
		})
	}

}]);