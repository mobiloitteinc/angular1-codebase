'use strict';

angular.module('Lawcunae.article_viewer', [])

.controller('articleViewerCtrl', ['$scope', '$stateParams', 'docs', 'commonService',function(sc, $stateParams, docs, commonService) {
    
    sc.articleID = commonService.stateChangeVal.value;
    
	if(commonService.stateChangeVal.exist){
		storeExistingList();
	}else
        getArticle(sc.articleID);

/******Function to fetch existing article viewer data if stored locally******/
    function storeExistingList(){
		var index=commonService.allTabsContent.article.findIndex((x) => x.keyword == commonService.stateChangeVal.value);
		if(index != -1){
			sc.articleDetail=commonService.allTabsContent.article[index].articleDetail;
			getLikeCounts();
		}else{
			getArticle(sc.articleID);
		}
	}

/******Getting article viewer detail from server******/
    function getArticle(article_id){
		docs.get_article_detail(article_id).then(function(objS){
			if(objS.data.responseCode == 200){
				sc.articleDetail=objS.data.data[0];
				commonService.allTabsContent.article.push({articleDetail:sc.articleDetail, keyword:article_id});
				getLikeCounts();
			}
		},function(objE){
			console.log('paragraph error => '+JSON.stringify(objE));
		})
	}

/******Managing like/dislike counts of the user******/
	function getLikeCounts(){
		sc.articleDetail._source.likeCount=0;
		sc.articleDetail._source.disLikeCount=0;
		sc.articleDetail._source.like=undefined;
		for(var k=0;k<sc.articleDetail._source.votes.length;k++){
			if(sc.articleDetail._source.votes[k].positive)
				sc.articleDetail._source.likeCount++;
			else
				sc.articleDetail._source.disLikeCount++;
			if(sc.articleDetail._source.votes[k].user_id == commonService.user_id){
				sc.articleDetail._source.like=sc.articleDetail._source.votes[k].positive;
			}
		}
	}

/******Open same tab when came from different article viewer******/
	sc.$on('sameTypeTab', function (event, args) {
		sc.articleID = commonService.stateChangeVal.value;
		if(commonService.stateChangeVal.exist){
			storeExistingList();
		}else
			getArticle(sc.articleID);
	});

/******Like article functionality******/
	sc.like_article = function(status){
		if(sc.articleDetail._source.like != status){
			var like_article_req = {
				"draft_id":sc.articleDetail._source.article_id,
				"votes":{
					"user_id":commonService.user_id,
					"date":new Date().getTime(),
					"positive":status
				}
			}
			docs.like_article(like_article_req).then(function(objS){
				sc.articleDetail=objS.data.data[0];
				getLikeCounts();
			},function(objE){
				console.log("like_article Err==>>",objE);
			})
		}
	}

/******Edit article detail functionality******/
	sc.editArticleDetail = function(status, text, source){
		if(commonService.user_id == sc.articleDetail._source.userId){
			if(status){
				sc.articleDetail._source.showEditDetail = status;
				sc.articleDetail._source.editDetText=text;
				sc.source=source;
			}else{
				sc.articleDetail._source[sc.source] = sc.articleDetail._source.editDetText;
				editArticleAPI('metadata');
			}
		}
	}

/******Cancel functionality when user don't want to edit metadata******/
	sc.cancelBox = function(source){
		if(source == 'metadata')
			sc.articleDetail._source.showEditDetail = false;
		else
			sc.articleDetail._source.showEditText = false;
	}

/******Article viewer CKEditor functionality when edit******/
	sc.articleTextClk = function(e, article_id, text, status){
		if(commonService.user_id == sc.articleDetail._source.userId){
			status?sc.articleDetail._source.edittext=sc.articleDetail._source.text:sc.articleDetail._source.text=CKEDITOR.instances['text'+sc.articleDetail._source.article_id].getData();
			if(!status){
				sc.articleDetail._source.showEditText = status;
				editArticleAPI('text');
				
			}else {
				sc.articleDetail._source.showEditText = status;
				if(!CKEDITOR.instances['text'+sc.articleDetail._source.article_id])
					CKEDITOR.replace( 'text'+sc.articleDetail._source.article_id, {
						removeButtons: 'Source',
					} );
			}	
		}
	}

/******API to update article viewer detail******/
	function editArticleAPI(source){
        var draft_req={	
                        "title":sc.articleDetail._source.title,
                        "author_name":sc.articleDetail._source.author_name,
                        "citation":sc.articleDetail._source.citation,
                        "publication_name":sc.articleDetail._source.publication_name,
                        "abstract":sc.articleDetail._source.abstract,
                        "user_id":commonService.user_id,
                        "text":sc.articleDetail._source.text,
						"tags":[],
						"draft_id":sc.articleDetail._source.article_id,
						"date":new Date().getTime()
                    };

		docs.add_draft(draft_req, 'edit_articles').then(function(objS){
            if(objS.data.responseCode == 200){
				sc.articleDetail=objS.data.data[0];
            	var index = commonService.allTabsContent.article.findIndex((x) => x.keyword == sc.articleDetail._source.article_id);
            	if(index!=-1)
					commonService.allTabsContent.article[index].articleDetail=sc.articleDetail;
				
				if(source == 'metadata')
					sc.articleDetail._source.showEditDetail = false;
				else
					CKEDITOR.instances['text'+sc.articleDetail._source.article_id].destroy();
            }
        },function(objE){
            console.log('paragraph error => '+JSON.stringify(objE));
        })
	}
        
}]);

