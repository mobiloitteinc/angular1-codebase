'use strict';

angular.module('Lawcunae.docViewer', [])

.controller('docViewerCtrl', ['$scope', 'user', 'storage', '$timeout', 'commonService', 'docs', '$sce', '$stateParams', '$modal', '$location', '$anchorScroll', '$state', '$rootScope',function(sc, user, storage, $timeout, commonService, docs, $sce, $stateParams, $modal, $location, $anchorScroll, $state, $rootScope) {

/******Watcher when anything updated regarding user******/
	$rootScope.$watch('userupdated',function(){
		sc.user_info = commonService.user_info;
		reloadTabs();
	});

	sc.activeTabIndex=-1;
	sc.tabType="homeTab";
	$state.go('side_menu.document_viewer.home');
	sc.$emit('sidePanelView', { page: 'homeTab', keyID:'' });
	
/******Restore all tabs of particular user after login******/
	function reloadTabs(){
		if(commonService.user_info != null){
			if(!commonService.tabReloadStatus){
				pushExistingTabs();
			}else{
				if(sc.tabArr == undefined){
					pushExistingTabs();
				}
			}
		}else{
			// sc.tabArr=[];
		}
	}

/******Pushing all tabs and manage their states******/
	function pushExistingTabs(){
		sc.tabArr=[];
		commonService.tabReloadStatus=true;
		for(var i=0;i<commonService.user_info.tabs.length;i++){
			if(commonService.user_info.tabs[i].type == 'search'){
				sc.tabArr.push({name:commonService.user_info.tabs[i].data.keyID+' - search results', closeIcon:false, pinIcon:false, pinned:false, tabSource:commonService.user_info.tabs[i].type, keyID:commonService.user_info.tabs[i].data.keyID, type:commonService.user_info.tabs[i].data.type, tab_id:commonService.user_info.tabs[i]._id});
			}else{
				sc.tabArr.push({name:commonService.user_info.tabs[i].data.title, closeIcon:false, pinIcon:false, pinned:false, tabSource:commonService.user_info.tabs[i].type, keyID:commonService.user_info.tabs[i].data.keyID, tab_id:commonService.user_info.tabs[i]._id});
			}
			if(i == commonService.user_info.tabs.length-1){
				reduceTitleLength();
			}
		}
	}

/******API to get the document detail******/
	function getParagraph(doc_id){
		docs.get_paragraphs({"document_id":doc_id}).then(function(objS){
			if(objS.data.responseCode == 200){
				commonService.allTabsContent.document.push({tabDetail:objS.data.data[0], keyword:doc_id});
			}
		},function(objE){
			console.log('paragraph error => '+JSON.stringify(objE));
		})
	}

/*********API to getting search listing results*******/
	function hitAPISearchList(keyword, pageIndex, insertType){
		var searchReq={
			search:keyword,
			filter:sc.filter
		};
		sc.docList=[];
		docs.serachDoc_pagination(searchReq, pageIndex).then(function(objS){
			if(objS.data.responseCode == 200){
				if(objS.data.data.length>0){
					sc.docList=objS.data.data;
					objS.data.courts=[];
					for(var i=0;i<sc.docList.length;i++){
						var matchesStr='';
						objS.data.courts.push(sc.docList[i]._source.courtName);
						if(sc.docList[i].highlight['paragraphs.text'] != undefined){
							for(var k=0;k<sc.docList[i].highlight['paragraphs.text'].length;k++){
								if(matchesStr != '')
									matchesStr=matchesStr+' ... ';
								matchesStr=matchesStr+sc.docList[i].highlight['paragraphs.text'][k];
							}
						}
						sc.docList[i].matches=matchesStr;
						if(i == sc.docList.length-1){
							sc.courtArr=objS.data.courts;
							if(insertType == 'new'){
								commonService.allTabsContent.search.push({docList:sc.docList, total:objS.data.total, keyword:keyword, courts:objS.data.courts});
							}
							console.log(JSON.stringify(commonService.allTabsContent));
						}
					}
				}else
					sc.docList=[];
			}
		},function(objE){
			console.log('doc list objE => '+JSON.stringify(objE));
		})
	}

/***********Adding tab functionality*************/
	sc.addTab = function(){
		sc.tabArr.push({name:'New Tab '+sc.tabArr.length, closeIcon:false, pinIcon:false, pinned:false, tabSource:'add'});
		sc.activeTabIndex=sc.tabArr.length-1;
		sc.tabType="noTab";
	}

/***********Closing tab functionality*************/
	sc.closeTab = function(index){
		var tab_req={"user_id":commonService.user_id,"tab_id":[sc.tabArr[index].tab_id]};
		docs.closeTab(tab_req).then(function(objS){
			if(objS.data.responseCode == 200){
				sc.tabArr.splice(index,1);
				sc.activeTabIndex=sc.tabArr.length-1;
				if(sc.activeTabIndex == -1){
					sc.tabType="homeTab";
					$state.go('side_menu.document_viewer.home');
					$timeout(function(){
						sc.$emit('sidePanelView', { page: 'homeTab', keyID:'' });
					},1000);
				}else
					sc.tabClick(sc.activeTabIndex);
			}
		},function(objE){
			console.log('paragraph error => '+JSON.stringify(objE));
		});
	}

/******Initialization of all static tabs******/
	sc.displayAdmin=false;
	sc.displayMyActivity=false;
	sc.displayProfile=false;
	sc.displayCitation=false;
	sc.displayUploadDoc=false;
	sc.displayDraftList=false;
	sc.displayDraftEditor=false;
	sc.displayPaymentHistory=false;

/******Close particular tabs******/
	sc.closeProfileTab = function(index){
		// index==-2?sc.displayProfile=false:sc.displayCitation=false;
		if(index == -2)
			sc.displayProfile=false;
		else if(index == -3)
			sc.displayCitation=false;
		else if(index == -4)
			sc.displayUploadDoc=false;
		else if(index == -5)
			sc.displayDraftList=false;
		else if(index == -6)
			sc.displayDraftEditor=false;
		else if(index == -7)
			sc.displayPaymentHistory=false;
		else if(index == -8)
			sc.displayMyActivity=false;
		else if(index == -9)
			sc.displayAdmin=false;

		sc.activeTabIndex=-1;
		sc.tabType="homeTab";
		$state.go('side_menu.document_viewer.home');
		sc.$emit('sidePanelView', { page: 'homeTab', keyID:'' });
	}

	sc.$on('staticTabClose', function (event, args) {
		sc.closeProfileTab(args.index);
	});

/***********Avoiding the restrictions on url*************/
	sc.trustAsHtml = function(html) {
		return $sce.trustAsHtml(html);
	}
	
/***********Tab on hover in functionality*************/
	sc.hoverIn = function(index){
		sc.tabArr[index].pinIcon=true;
		if(!sc.tabArr[index].pinned)
			sc.tabArr[index].closeIcon=true;
	}

/***********Tab on hover out functionality*************/
	sc.hoverOut = function(index){
		sc.tabArr[index].pinIcon=false;
		sc.tabArr[index].closeIcon=false;
	}

/***********pin and unpin functionality*************/
	sc.pinUnpin = function(index){
		sc.tabArr[index].pinned ? sc.tabArr[index].pinned=false : sc.tabArr[index].pinned=true;
		sc.tabArr[index].pinIcon=false;
		sc.tabArr[index].closeIcon=false;
		var obj=angular.copy(sc.tabArr[index]);
		sc.tabArr.splice(index,1);
		var flag=false;
		for(var i=0;i<sc.tabArr.length;i++){
			if(!sc.tabArr[i].pinned){
				flag=true;
				sc.tabArr.splice(i,0,obj);
				sc.activeTabIndex=i;
				sc.tabClick(i);
				break;
			}
		}
		if(!flag){
			sc.tabArr.push(obj);
			sc.activeTabIndex=sc.tabArr.length-1;
			sc.tabClick(sc.activeTabIndex);
		}

	}

	sc.$on('openCaseOfDoc', function (event, args) {
		sc.caseViewerClk({id:args.id, title:args.title});
	});
	
/*********Event to handle when user seatching on text field*******/
	sc.$on('searchDoc', function (event, args) {
		searchDocOnEnter(args);
	});

	sc.$on('menuOpen', function (event, args) {
		if(args.index == -2){
			// sc.displayProfile=true;
			sc.profileViewerClk({_source:{user_id:commonService.user_id, name:'Profile'}});
		}else if(args.index == -3)
			sc.displayCitation=true;
		else if(args.index == -4)
			sc.displayUploadDoc=true;
		else if(args.index == -5)
			sc.displayDraftList=true;
		else if(args.index == -6)
			sc.displayDraftEditor=true;
		else if(args.index == -7)
			sc.displayPaymentHistory=true;
		else if(args.index == -8)
			sc.displayMyActivity=true;
		else if(args.index == -9)
			sc.displayAdmin=true;

		if(args.index != -2){
			sc.activeTabIndex=args.index;
			sc.tabType=args.name;
			$state.go('side_menu.document_viewer.'+args.page);
			sc.$emit('sidePanelView', { page: args.name, keyID:'' });
			closeSidebars();
		}
	});

/******Close sidebar if open on tab change******/
	function closeSidebars(){
		if($('.firac-accordian-block.commonsidewidth').hasClass('toggled')){
			$("#firac-accordian-block").removeClass("toggled");
		}
		if($('.firac-facts.commonsidewidth').hasClass('toggled')){
			$(".firac-facts.commonsidewidth").removeClass("toggled");
		}
		if($("#list-cases-sidebar-block").hasClass("toggled")){
			$("#list-cases-sidebar-block").removeClass("toggled");
		}
		if($('.filter-document-sidebar').hasClass('toggled')){
			$(".filter-document-sidebar").removeClass("toggled");
		}
		if($('#comment-sidebar-block').hasClass('toggled')){
			$("#comment-sidebar-block").removeClass("toggled");
		}
	}

/******Search doc functionality on when search from top******/
	function searchDocOnEnter(args){
		sc.message = args.name;
		sc.type = args.type;
		sc.tabType="searchTab";
		sc.$emit('sidePanelView', { page: 'searchTab', keyID:sc.message, type: sc.type});
		var index=sc.tabArr.findIndex((x) => x.name.split(' - ')[0] == sc.message);
		if(index==-1){
			commonService.stateChangeVal={exist:false, value:{name:sc.message, type:sc.type}};
			var tab_req={"user_id":commonService.user_id,"type":"search","data":{keyID:sc.message, title:sc.message, type:sc.type},"date":new Date().getTime()};
			docs.addTab(tab_req).then(function(objS){
				if(objS.data.responseCode == 200){
					sc.tabArr.push({name:sc.message+' - search results', closeIcon:false, pinIcon:false, pinned:false, tabSource:'search', keyID:sc.message, type:sc.type, tab_id:objS.data.data});
					sc.activeTabIndex=sc.tabArr.length-1;
					reduceTitleLength();
				}
			},function(objE){
				console.log('paragraph error => '+JSON.stringify(objE));
			})
		}else{
			commonService.stateChangeVal={exist:true, value:{name:sc.message, type:sc.type}};
			sc.activeTabIndex=index;
		}
		if($state.current.name == 'side_menu.document_viewer.search'){
			sc.$broadcast('sameTypeTab', {id:commonService.stateChangeVal});
		}else{
			$state.go('side_menu.document_viewer.search');
		}
	}

/*********Particular tab click functionality*******/
	sc.tabClick = function(index){
		if($('.global-filter-box').hasClass('active')){
			$(".global-filter-box").toggleClass("active");
		}
		closeSidebars();
		sc.activeTabIndex=index;
		if(sc.tabArr[index].tabSource == 'add'){
			sc.tabType="noTab";
			sc.$emit('sidePanelView', { page: 'noTab', keyID:'' });
		}else if(sc.tabArr[index].tabSource == 'search'){
			sc.tabType="searchTab";
			sc.$emit('sidePanelView', { page: 'searchTab', keyID:sc.tabArr[index].keyID, type:sc.tabArr[index].type});
			commonService.stateChangeVal={exist:true, value:{name:sc.tabArr[index].keyID, type:sc.tabArr[index].type}};
			if($state.current.name == 'side_menu.document_viewer.search'){
				sc.$broadcast('sameTypeTab', {id:commonService.stateChangeVal});
			}else{
				$state.go('side_menu.document_viewer.search');
			}
		}else if(sc.tabArr[index].tabSource == 'document'){
			sc.tabType="documentTab";
			sc.$emit('sidePanelView', { page: 'documentTab', keyID:sc.tabArr[index].keyID });
			commonService.stateChangeVal={exist:true, value:sc.tabArr[index].keyID};
			if($state.current.name == 'side_menu.document_viewer.document_detail'){
				sc.$broadcast('sameTypeTab', {id:commonService.stateChangeVal});
			}else{
				$state.go('side_menu.document_viewer.document_detail');
			}
		}else if(sc.tabArr[index].tabSource == 'caseViewer'){
			sc.tabType="caseViewerTab";
			sc.$emit('sidePanelView', { page: 'caseViewerTab', keyID:sc.tabArr[index].keyID });
			commonService.stateChangeVal={exist:true, value:sc.tabArr[index].keyID};
			if($state.current.name == 'side_menu.document_viewer.caseViewer'){
				sc.$broadcast('sameTypeTab', {id:commonService.stateChangeVal});
			}else{
				$state.go('side_menu.document_viewer.caseViewer');
			}
		}else if(sc.tabArr[index].tabSource == 'profile'){
			sc.tabType="profileTab";
			sc.$emit('sidePanelView', { page: 'profileTab', keyID:sc.tabArr[index].keyID });
			commonService.stateChangeVal={exist:true, value:sc.tabArr[index].keyID};
			if($state.current.name == 'side_menu.document_viewer.profile'){
				sc.$broadcast('sameTypeTab', {id:commonService.stateChangeVal});
			}else{
				$state.go('side_menu.document_viewer.profile');
			}
		}else if(sc.tabArr[index].tabSource == 'article'){
			sc.tabType="articleTab";
			sc.$emit('sidePanelView', { page: 'articleTab', keyID:sc.tabArr[index].keyID });
			commonService.stateChangeVal={exist:true, value:sc.tabArr[index].keyID};
			if($state.current.name == 'side_menu.document_viewer.article_viewer'){
				sc.$broadcast('sameTypeTab', {id:commonService.stateChangeVal});
			}else{
				$state.go('side_menu.document_viewer.article_viewer');
			}
		}
	}

	sc.aClick = function(){
		console.log('a clicked');
	}

/******Emit function when update from search listing******/
	sc.$on('searchEmit', function (event, args) {
		var tab_req={"tab_id":sc.tabArr[sc.activeTabIndex].tab_id,"value":args.name};
		docs.updateTab(tab_req).then(function(objS){
			if(objS.data.responseCode == 200){
				sc.tabArr[sc.activeTabIndex].name=args.name+' - search results';
				sc.tabArr[sc.activeTabIndex].keyID=args.name;
				reduceTitleLength();
			}
		},function(objE){
			console.log('paragraph error => '+JSON.stringify(objE));
		});
	});

/******Emit function when update in document viewer******/
	sc.$on('docDetailEmit', function (event, args) {
		if(args.source == 'direct'){
			var index=sc.tabArr.findIndex((x) => x.keyID == args.link);
			if(index==-1){
				var newTabReq={_source:{id:parseInt(args.link), title:'Loading...'}};
				sc.docViewerClk(newTabReq);
			}else{
				sc.tabClick(index);
			}
		}else if(args.source == 'indirect'){
			if(sc.activeTabIndex!=-1 && sc.activeTabIndex!=-2 && sc.activeTabIndex!=-3)
				sc.tabArr[sc.activeTabIndex].name=args.link;
			reduceTitleLength();
		}else if(args.source == 'annotation'){
			var index=sc.tabArr.findIndex((x) => x.keyID == args.link);
			if(index==-1){
				var newTabReq={_source:{id:parseInt(args.link), title:args.text}};
				sc.docViewerClk(newTabReq);
			}else{
				sc.tabClick(index);
			}
		}else if(args.source == 'case'){
			console.log('args => '+JSON.stringify(args));
			var index=sc.tabArr.findIndex((x) => x.keyID == args.link);
			if(index==-1){
				var newTabReq={id:parseInt(args.link), title:args.text};
				sc.caseViewerClk(newTabReq);
			}else{
				sc.tabClick(index);
			}
		}else if(args.source == 'article'){
			var index=sc.tabArr.findIndex((x) => x.keyID == args.link);
			if(index==-1){
				var newTabReq={_source:{article_id:parseInt(args.link), title:args.text}};
				sc.articleViewerClk(newTabReq);
			}else{
				sc.tabClick(index);
			}
		}else if(args.source == 'profile'){
			var index=sc.tabArr.findIndex((x) => x.keyID == args.id);
			if(index==-1){
				var newTabReq={_source:{user_id:parseInt(args.id), name:args.text}};
				sc.profileViewerClk(newTabReq);
			}else{
				sc.tabClick(index);
			}
		}
	});

/******Emit function to open document viewer when click on any notification******/
	sc.$on('openDocOnNoti', function (event, args) {
		var index=sc.tabArr.findIndex((x) => x.keyID == args.id);
		if(index==-1){
			var newTabReq={_source:{id:parseInt(args.id), title:'Loading...'}};
			sc.docViewerClk(newTabReq);
		}else{
			sc.tabClick(index);
		}
	});

/*********Particular search list click functionality*******/
	sc.docClick = function(docReq){
		if(docReq._type == 'indian_kanoon_new')
		// if(docReq._type == 'lawcunae_document')
			sc.docViewerClk(docReq);
		else if(docReq._type == 'lawcunae_case')
			sc.caseViewerClk({id:docReq._source.case_id, title:docReq._source.title});
		else if(docReq._type == 'lawcunae_user')
			sc.profileViewerClk(docReq);
		else if(docReq._type == 'lawcunae_article')
			sc.articleViewerClk(docReq);
	}

/*********document viewer of particular document functionality*******/
	sc.articleViewerClk = function(docReq){
		sc.tabType="articleTab";
		sc.$emit('sidePanelView', { page: 'articleTab', keyID:docReq._source.article_id });
		var index=sc.tabArr.findIndex((x) => x.keyID == docReq._source.article_id);
		if(index==-1){
			var tab_req={"user_id":commonService.user_id,"type":"article","data":{keyID:docReq._source.article_id, title:docReq._source.title},"date":new Date().getTime()};
			docs.addTab(tab_req).then(function(objS){
				if(objS.data.responseCode == 200){
					sc.tabArr.push({name:docReq._source.title, closeIcon:false, pinIcon:false, pinned:false, tabSource:'article', keyID:docReq._source.article_id, tab_id:objS.data.data});
					sc.activeTabIndex=sc.tabArr.length-1;
					reduceTitleLength();
				}
			},function(objE){
				console.log('paragraph error => '+JSON.stringify(objE));
			})
		}else{
			sc.activeTabIndex=index;
		}
		commonService.stateChangeVal={exist:false, value:docReq._source.article_id};
		if($state.current.name == 'side_menu.document_viewer.article_viewer'){
			sc.$broadcast('sameTypeTab', {id:commonService.stateChangeVal});
		}else{
			$state.go('side_menu.document_viewer.article_viewer');
		}
	}

/*********document viewer of particular document functionality*******/
	sc.profileViewerClk = function(docReq){
		sc.tabType="profileTab";
		sc.$emit('sidePanelView', { page: 'profileTab', keyID:docReq._source.user_id });
		var index=sc.tabArr.findIndex((x) => x.keyID == docReq._source.user_id);
		if(index==-1){
			var tab_req={"user_id":commonService.user_id,"type":"profile","data":{keyID:docReq._source.user_id, title:docReq._source.name},"date":new Date().getTime()};
			docs.addTab(tab_req).then(function(objS){
				if(objS.data.responseCode == 200){
					sc.tabArr.push({name:docReq._source.name, closeIcon:false, pinIcon:false, pinned:false, tabSource:'profile', keyID:docReq._source.user_id, tab_id:objS.data.data});
					sc.activeTabIndex=sc.tabArr.length-1;
					reduceTitleLength();
				}
			},function(objE){
				console.log('paragraph error => '+JSON.stringify(objE));
			})
		}else{
			sc.activeTabIndex=index;
		}
		commonService.stateChangeVal={exist:false, value:docReq._source.user_id};
		if($state.current.name == 'side_menu.document_viewer.profile'){
			sc.$broadcast('sameTypeTab', {id:commonService.stateChangeVal});
		}else{
			$state.go('side_menu.document_viewer.profile');
		}
	}

/*********document viewer of particular document functionality*******/
	sc.docViewerClk = function(docReq){
		sc.tabType="documentTab";
		sc.$emit('sidePanelView', { page: 'documentTab', keyID:docReq._source.id });
		var index=sc.tabArr.findIndex((x) => x.keyID == docReq._source.id);
		if(index==-1){
			var tab_req={"user_id":commonService.user_id,"type":"document","data":{keyID:docReq._source.id, title:docReq._source.title},"date":new Date().getTime()};
			docs.addTab(tab_req).then(function(objS){
				if(objS.data.responseCode == 200){
					sc.tabArr.push({name:docReq._source.title, closeIcon:false, pinIcon:false, pinned:false, tabSource:'document', keyID:docReq._source.id, tab_id:objS.data.data});
					sc.activeTabIndex=sc.tabArr.length-1;
					reduceTitleLength();
				}
			},function(objE){
				console.log('paragraph error => '+JSON.stringify(objE));
			})
		}else{
			sc.activeTabIndex=index;
		}
		commonService.stateChangeVal={exist:false, value:docReq._source.id};
		if($state.current.name == 'side_menu.document_viewer.document_detail'){
			sc.$broadcast('sameTypeTab', {id:commonService.stateChangeVal});
		}else{
			$state.go('side_menu.document_viewer.document_detail');
		}
	}

/*********Case viewer of particular document functionality*******/
	sc.caseViewerClk = function(docReq){
		console.log('docReq => '+JSON.stringify(docReq));
		sc.tabType="caseViewerTab";
		sc.$emit('sidePanelView', { page: 'caseViewerTab', keyID:docReq.id });
		var index=sc.tabArr.findIndex((x) => x.keyID == docReq.id && x.tabSource == 'caseViewer');
		if(index==-1){
			var tab_req={"user_id":commonService.user_id,"type":"caseViewer","data":{keyID:docReq.id, title:docReq.title},"date":new Date().getTime()};
			docs.addTab(tab_req).then(function(objS){
				if(objS.data.responseCode == 200){
					sc.tabArr.push({name:docReq.title, closeIcon:false, pinIcon:false, pinned:false, tabSource:'caseViewer', keyID:docReq.id, tab_id:objS.data.data});
					sc.activeTabIndex=sc.tabArr.length-1;
					reduceTitleLength();
				}
			},function(objE){
				console.log('paragraph error => '+JSON.stringify(objE));
			})
		}else{
			sc.activeTabIndex=index;
		}
		commonService.stateChangeVal={exist:false, value:docReq.id};  
		if($state.current.name == 'side_menu.document_viewer.caseViewer'){
			sc.$broadcast('sameTypeTab', {id:commonService.stateChangeVal});
		}else{
			$state.go('side_menu.document_viewer.caseViewer');
		}  
	}

	sc.staticTabClick = function(page, index, pageName){
		sc.tabType=page;
		sc.$emit('sidePanelView', { page: page, keyID:'' });
		sc.activeTabIndex=index;
		$state.go('side_menu.document_viewer.'+pageName);
		closeSidebars();
	}

	function reduceTitleLength(){
		for(var i=0;i<sc.tabArr.length;i++){
			sc.tabArr[i].titleName = sc.tabArr[i].name.substr(0,(120/((sc.tabArr.length)+1)));
		}
	}

/*********Function to close all tabs at one time***************/
	sc.context_div = "<ul id='contextmenu-node' class='dropdown' ng-click='closeAllTab()'><li class='contextmenu-item'><i class='fa-close'></i>Close All Tabs</li></ul>"
	sc.closeAllTab = function() {
		if (sc.tabArr.length > 0) {
			var tempIDArr=[];
			for (var i=0;i<sc.tabArr.length;i++) {
				tempIDArr.push(sc.tabArr[i].tab_id);
				if(i == sc.tabArr.length-1){
					var tab_req={"user_id":commonService.user_id,"tab_id":tempIDArr};
					docs.closeTab(tab_req).then(function(objS){
						if(objS.data.responseCode == 200){
							closeStaticTabs();
						}
					},function(objE){
						console.log('paragraph error => '+JSON.stringify(objE));
					});
				}
			}
		}else
			closeStaticTabs();
	};

	function closeStaticTabs(){
		sc.displayProfile = false;
		sc.displayCitation = false;
		sc.displayUploadDoc = false;
		sc.displayDraftList = false;
		sc.displayDraftEditor = false;
		sc.displayPaymentHistory = false;
		sc.displayMyActivity = false;
		sc.displayAdmin = false;
		sc.tabArr = [];
		commonService.allTabsContent={search:[], document:[], case:[], graph:[], profile:[], article:[]};
		sc.activeTabIndex = -1;
		sc.tabType = "homeTab";
		$state.go('side_menu.document_viewer.home');
		sc.$emit('sidePanelView', { page: 'homeTab', keyID: '' });
		$("#contextmenu-node").remove();
	}

}]);