'use strict';

angular.module('Lawcunae.draft_editor', [])

.controller('draftEditorCtrl', ['$timeout', '$state', '$scope', 'docs', 'commonService', '$sce', 'API_URL', 'storage', 'alertify', function($timeout, st, sc, docs, commonService, $sce, API_URL, storage, alertify) {
    $timeout(function(){
    /******CKEditor initialization******/
        if(st.current.name == 'side_menu.document_viewer.draft_editor'){
            if(!CKEDITOR.instances['draft_edit_write']){
                CKEDITOR.replace( 'draft_edit_write', {
                    removeButtons: 'Source',
                } );
            }else{
                CKEDITOR.instances['draft_edit_write'].removeAllListeners();
                CKEDITOR.remove(CKEDITOR.instances['draft_edit_write']);
                CKEDITOR.replace( 'draft_edit_write', {
                    removeButtons: 'Source',
                } );
            }

            CKEDITOR.instances["draft_edit_write"].on('key', function() {
                $timeout(function(){
                    sc.copyVal=false;
                },500);
            });

        /******Citation id paste functionality******/
            var editor = CKEDITOR.instances['draft_edit_write'];
            editor.on('paste', function(evt) {
                $timeout(function(){
                    var str=evt.editor.getData().replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&quot;/g, '"');
                    evt.editor.setData(str);
                },100)
                
            }, editor.element.$);

        /******Checking whether came for new draft or existing******/
            if(commonService.draftType == 'new'){
                sc.draft={title:"", preview:"", published:true, author_name:"", citation:"", publication_name:"", abstract:""};
                sc.copyVal=true;
            }else if(commonService.draftType != undefined){
                var draft_req={"draft_id":commonService.draftType,"user_id":commonService.user_id}
                docs.add_draft(draft_req, 'get_draft').then(function(objS){
                    if(objS.data.responseCode == 200){
                        var draftDet=objS.data.data.drafts[0];
                        if(CKEDITOR.instances['draft_edit_write'] != undefined){
                            CKEDITOR.instances['draft_edit_write'].setData(draftDet.text);
                            sc.draft={title:draftDet.title, author_name:draftDet.author_name, citation:draftDet.citation, publication_name:draftDet.publication_name, abstract:draftDet.abstract, published:draftDet.published, date:draftDet.date};
                            sc.copyVal=true;
                        }
                    }
                },function(objE){
                    console.log('add_draft error => '+JSON.stringify(objE));
                })
            }
        }
    },500);

/******Select2 initialization for document tags******/
    $(document).ready(function() {
        $(".draft_tag").select2({
            placeholder: "Search for document tags",
            ajax: {
                url: API_URL+"documentActivity/auto_suggest",
                dataType: 'json',
                beforeSend: function (xhr) { 
                    xhr.setRequestHeader("Authorization", storage.get('token'));
                },
                delay: 250,
                data: function (params) {
                    return {
                        suggestion: params.term
                    };
                },
                processResults: function (data, params) {
                return {
                        results: data.data
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; },
            minimumInputLength: 1,
            templateResult: formatRepoDoc,
            templateSelection: formatRepoSelectionDoc
        }).trigger('change');
    });

    function formatRepoDoc (repo) {
		if (repo.loading) return "Loading terms..";
		    return "<div>"+repo.tag+"</div>";
	}

	function formatRepoSelectionDoc (repo) {
		return repo.tag;
	}

/******Save draft functionality******/
    sc.saveDraft = function(type){
        var doc_tagArr=[];
        for(var i=0;i<$(".draft_tag").select2('data').length;i++){
			doc_tagArr.push($(".draft_tag").select2('data')[i].tag);
        }
        var currentDate=new Date().getTime();
        var draft_req={	
                        "title":sc.draft.title,
                        "author_name":sc.draft.author_name,
                        "citation":sc.draft.citation,
                        "publication_name":sc.draft.publication_name,
                        "abstract":sc.draft.abstract,
                        "user_id":commonService.user_id,
                        "text":CKEDITOR.instances['draft_edit_write'].getData(),
                        "tags":doc_tagArr
                    };
        if(type == 'get_draft_id'){
            if(commonService.draftType == 'new'){
                var currentDate=new Date().getTime();
                draft_req.date=currentDate;
                var end_point=type;
            }else{
                var currentDate=sc.draft.date;
                draft_req.draft_id=commonService.draftType;
                draft_req.date=sc.draft.date;
                var end_point='update_draft';
            }
        }else{
            var currentDate=sc.draft.date;
            draft_req={"user_id":commonService.user_id, "draft_id":commonService.draftType, "date":sc.draft.date};
            var end_point=type;
        }

        if(end_point != 'publish_draft'){
            if (!navigator.onLine) {
                alertify.error('Please check your network connection')
            } else if (draft_req.title == '') {
                alertify.error("Please enter title")
            } else if (draft_req.author_name == '') {
                alertify.error("Please enter author name")
            } else{
                draftAPI(currentDate, draft_req, end_point);
            }
        }else{
            draftAPI(currentDate, draft_req, end_point);
        }
    }

/******Publish draft functionality******/
    function draftAPI(currentDate, draft_req, end_point){
        docs.add_draft(draft_req, end_point).then(function(objS){
            if(objS.data.responseCode == 200){
                var updateList=objS.data.data.drafts;
                if(end_point != 'publish_draft'){
                    var index=updateList.findIndex((x) => x.date == currentDate);
                    if(index != -1){
                        var draftDet=updateList[index];
                        commonService.draftType=draftDet.draft_id;
                        if(CKEDITOR.instances['draft_edit_write'] != undefined){
                            CKEDITOR.instances['draft_edit_write'].setData(draftDet.text);
                            sc.draft={title:draftDet.title, author_name:draftDet.author_name, citation:draftDet.citation, publication_name:draftDet.publication_name, abstract:draftDet.abstract, published:draftDet.published, date:draftDet.date};
                            sc.copyVal=true;
                        }
                    }
                }
                if(end_point == 'publish_draft'){
                    alertify.success('Your draft has been published');
                    sc.$emit('staticTabClose', { index: -6 });
                }
            }
        },function(objE){
            console.log('paragraph error => '+JSON.stringify(objE));
        })
    }

/******Populating data when click on preview******/
    sc.previewClk = function(){
        sc.draft.preview = CKEDITOR.instances['draft_edit_write'].getData();
    }

/***********Avoiding the restrictions on url*************/
    sc.trustAsHtml = function(html) {
		return $sce.trustAsHtml(html);
    }
    
    sc.editedDraft = function(){
        sc.copyVal=false;
    }

/******Checking whether any changes in CKEditor******/
    $('.draft_tag').on('select2:select', function (e) {
        $timeout(function(){
            sc.copyVal=false;
        },500);
        
    });

    $('.draft_tag').on('select2:unselect', function (e) {
        $timeout(function(){
            sc.copyVal=false;
        },500);
    });

}]);