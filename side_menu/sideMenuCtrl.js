'use strict';

angular.module('Lawcunae.sideMenu', [])

.controller('sideMenuCtrl', ['$sce', '$http', '$scope', '$timeout', '$state', 'docs', '$interval', '$q', 'commonService', '$rootScope', 'storage', 'API_URL', 'user', 'socialLoginService', 'alertify' , function($sce, $http, sc, $timeout, $state, docs, $interval, $q, commonService, $rootScope, storage, API_URL, user, socialLoginService, alertify) {

/***********Code to expand and collapse the side panel*************/
	jQuery(document).ready(function($)
	{
		$timeout(function(){
			if( ! $.isFunction($.fn.dxChart))
				$(".dx-warning").removeClass('hidden');
			$('[data-toggle="sidebar"]').on('click', function(){
				$('.sidebar-menu').toggleClass('collapsed');
			});
		},100);
	});

	function async(u, c) {
		var d = document, t = 'script',
			o = d.createElement(t),
			s = d.getElementsByTagName(t)[0];
		o.src = '//' + u;
		if (c) { o.addEventListener('load', function (e) { c(null, e); }, false); }
		s.parentNode.insertBefore(o, s);
	}

	$rootScope.$watch('userupdated',function(){
		sc.user_info = commonService.user_info;
	})

	sc.user_id = commonService.user_id;
	sc.searchVal='';
	sc.selectType='default';
	// sc.filter={court:''};
	sc.firac={ownFirac:false, facts:'', issue:'', rationale:'', application:'', conclusion:'', comment:'', showCommentFirac:false};
	sc.searchDocument = function(){
		$timeout(function(){
			sc.$broadcast('searchDoc', {name:sc.searchVal, type:sc.selectType});
			sc.searchVal='';
			sc.selectType='default';
		},100);
	}

	sc.$on('sidePanelView', function (event, args) {
		// sc.filter.court='';
		sc.page = args.page;
		sc.keyID = args.keyID;
		sc.paraID = args.paraID;
		sc.type = args.type;
		if(sc.page=='searchTab'){
			// $('.right-sidebar').addClass('search-result');
			$timeout(function(){
				$(".search-tab-filter").addClass("active");
				$(".main-content").addClass("search-filter-content"); 
				filterCourt();
				sc.filter={containing:'', type:sc.type};
				$(".datepickerFrom").click(function(){
					$("#fromFilter").datepicker("show");
				});
				$(".datepickerTo").click(function(){
					$("#toFilter").datepicker("show"); 
				});
			},100);

		}else{
			// $('.right-sidebar').removeClass('search-result');
			$timeout(function(){
				$(".search-tab-filter").removeClass("active");
				$(".main-content").removeClass("search-filter-content"); 
			},100);
			if(sc.page=='documentTab'){
				// sc.updateApiStatus=true;
				// sc.sourceHandle=true;
				// console.log($('.firac-accordian-block.commonsidewidth').hasClass('toggled'));
				$timeout(function(){
					// console.log($('.firac-accordian-block.commonsidewidth').hasClass('toggled'));
					if($('.firac-accordian-block.commonsidewidth').hasClass('toggled')){
						sc.updateApiStatus=true;
						sc.sourceHandle=true;
						intervalOfSeconds();
						getParaComments(args.keyID, args.paraID, 'emit');
					}	
				},500);
			}
		}
	});

	$(document).on('click', '.addinsight', function(e){
	   e.preventDefault();
	   $(this).siblings(".firac-form").toggleClass("show");
		//   console.log($(this).siblings(".firac-form").hasClass("show"))
	   if($(this).siblings(".firac-form").hasClass("show")){
			sc.updateApiStatus=false;
			annotationTag();
	   }else
			sc.updateApiStatus=true;
	   
	});

	sc.linkClick = function(link, text){
		if(link.split('/')[0] == 'document'){
			link=link.split('/')[1];
			$("#firac-accordian-block").toggleClass("toggled");
			sc.$broadcast('docDetailEmit', {link:link, source:'annotation', text:text});
		}
	}

	sc.addAnnot = function(paragraph){
		if(!CKEDITOR.instances['para'+paragraph.paragraph_id]){
			CKEDITOR.replace( 'para'+paragraph.paragraph_id, {
				removeButtons: 'Source',
			} );
		}
		var editor = CKEDITOR.instances['para'+paragraph.paragraph_id];
		editor.on('paste', function(evt) {
			$timeout(function(){
				var str=evt.editor.getData().replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&quot;/g, '"');
				evt.editor.setData(str);
			},100)
			
		}, editor.element.$);
	}

	function annotationTag(){
		async('cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js', function() {
			$(".annot_select2").select2({
				placeholder: "Search for document tags",
				ajax: {
					url: API_URL+"documentActivity/auto_suggest",
					dataType: 'json',
					beforeSend: function (xhr) { 
						xhr.setRequestHeader("Authorization", storage.get('token'));
					},
					delay: 250,
					data: function (params) {
						return {
							suggestion: params.term
						};
					},
					processResults: function (data, params) {
					return {
							results: data.data
						};
					},
					cache: true
				},
				escapeMarkup: function (markup) { return markup; },
				minimumInputLength: 1,
				templateResult: formatRepo,
				templateSelection: formatRepoSelection
			}).data('select2').$dropdown.addClass('annot_popup').trigger('change');
		});
	}

	function formatRepo (repo) {
		if (repo.loading) return "Loading terms..";
		return "<div>"+repo.tag+"</div>";
	}

	function formatRepoSelection (repo) {
		return repo.tag;
	}

	sc.submitAnnotation = function(paragraph){
		var id_para;
		paragraph._id==undefined?id_para=paragraph.paragraph_id:id_para=paragraph._id;
		var annotation_req={
			"document_id":sc.keyID,
			"paragraph_id":paragraph.paragraph_id,
			"annotation":{
					"user_id":commonService.user_id,
					"date": new Date().getTime(),
					 "title":paragraph.addAnnoTitle==undefined?'':paragraph.addAnnoTitle,
					"text": CKEDITOR.instances['para'+paragraph.paragraph_id].getData(),
					"highlight":["This document excludes cases of software"]
				}
			}
			
		docs.addAnnotation(annotation_req).then(function(objS){
			if(objS.data.responseCode == 200){
				sc.updateApiStatus=true;
				getParaComments(sc.keyID, paragraph.paragraph_id, 'emit');
			}
		},function(objE){
			console.log('add annotation error => '+JSON.stringify(objE));
		})
	}

	function getParaComments(docID, paraID, source){
		$q.all([docs.get_para_comments(docID), docs.get_paragraphs({"document_id":docID})]).then(function(objS){
			if(objS[0].data.responseCode == 200){
				if(objS[0].data.data==null){
					for(var i=0;i<objS[1].data.data[0]._source.paragraphs.length;i++){
						if(i == 0){
							sc.getParaComm={document_id:docID, paragraphs:[{paragraph_id:objS[1].data.data[0]._source.paragraphs[i].paragraph_Id, annotations:[]}]};
						}else{
							sc.getParaComm.paragraphs.push({paragraph_id:objS[1].data.data[0]._source.paragraphs[i].paragraph_Id, annotations:[]});;
						}
						if(i == objS[1].data.data[0]._source.paragraphs.length-1){
							if(source=='emit')
								commentParaCounts(objS[1], 'emit', paraID);
							else
								commentParaCounts(objS[1], 'global', paraID);
						}
					}
					/*if(source=='emit'){
						sc.getParaComm={document_id:docID, paragraphs:[{paragraph_id:paraID, annotations:[]}]};
						commentParaCounts(objS[1], 'emit', paraID);
					}else{
						sc.getParaComm={document_id:docID, paragraphs:[]};
					}*/
				}else{
					sc.getParaComm=objS[0].data.data;
					for(var i=0;i<objS[1].data.data[0]._source.paragraphs.length;i++){
						var paraIndex=sc.getParaComm.paragraphs.findIndex((x) => x.paragraph_id == objS[1].data.data[0]._source.paragraphs[i].paragraph_Id);
						if(paraIndex == -1)
							sc.getParaComm.paragraphs.push({paragraph_id:objS[1].data.data[0]._source.paragraphs[i].paragraph_Id, annotations:[]});
						if(i == objS[1].data.data[0]._source.paragraphs.length-1){
							if(source=='emit')
								commentParaCounts(objS[1], 'emit', paraID);
							else
								commentParaCounts(objS[1], 'global', '');	
						}
					}
					/*if(source=='emit'){
						var index=objS[0].data.data.paragraphs.findIndex((x)=>x.paragraph_id == paraID);
						if(index==-1){
							sc.getParaComm.paragraphs.push({paragraph_id:paraID, annotations:[]});
							commentParaCounts(objS[1], 'emit', paraID);
						}else{
							commentParaCounts(objS[1], 'emit', paraID);
						}
					}else{
						commentParaCounts(objS[1], 'global', '');
					}*/
				}
			}
		},function(objE){
			console.log('get_para_comments error => '+JSON.stringify(objE));
		})
	}

	function commentParaCounts(objParam, source, paraID){
		for(var i=0;i<sc.getParaComm.paragraphs.length;i++){
			sc.getParaComm.paragraphs[i].collapse=false;	
			if(source == 'emit'){
				if(paraID == sc.getParaComm.paragraphs[i].paragraph_id)
					sc.getParaComm.paragraphs[i].collapse=true;	
			}
			var index=objParam.data.data[0]._source.paragraphs.findIndex((x) => x.paragraph_Id == sc.getParaComm.paragraphs[i].paragraph_id);
			if(index!=-1)
				sc.getParaComm.paragraphs[i].paragraph_number=objParam.data.data[0]._source.paragraphs[index].paragraph_number;
			for(var j=0;j<sc.getParaComm.paragraphs[i].annotations.length;j++){
				sc.getParaComm.paragraphs[i].annotations[j].likeCount=0;
				sc.getParaComm.paragraphs[i].annotations[j].disLikeCount=0;
				sc.getParaComm.paragraphs[i].annotations[j].like=undefined;
				for(var k=0;k<sc.getParaComm.paragraphs[i].annotations[j].votes.length;k++){
					if(sc.getParaComm.paragraphs[i].annotations[j].votes[k].positive)
						sc.getParaComm.paragraphs[i].annotations[j].likeCount++;
					else
						sc.getParaComm.paragraphs[i].annotations[j].disLikeCount++;
					if(sc.getParaComm.paragraphs[i].annotations[j].votes[k].user_id == commonService.user_id){
						sc.getParaComm.paragraphs[i].annotations[j].like=sc.getParaComm.paragraphs[i].annotations[j].votes[k].positive;
					}
				}
			}
			if(i == sc.getParaComm.paragraphs.length-1){
				// console.log('getParaComm => '+JSON.stringify(sc.getParaComm));
			}
		}
	}

	var interval = 0;
	sc.nestedAnnoClk = function(){
		if($('.firac-accordian-block.commonsidewidth').hasClass('toggled')){
			sc.sourceHandle=false;
			$interval.cancel(interval);
			sc.$broadcast('updateDoc', {name:sc.keyID});
		}else{
			sc.sourceHandle=false;
			getParaComments(sc.keyID, sc.paraID, '');
			sc.updateApiStatus=true;
			intervalOfSeconds();
		}
	}

	function intervalOfSeconds(){
		interval = $interval(function(){
			if(sc.updateApiStatus){
				if($('.firac-accordian-block.commonsidewidth').hasClass('toggled')){
					sc.sourceHandle?getParaComments(sc.keyID, sc.paraID, 'emit'):getParaComments(sc.keyID, sc.paraID, '');
					// getParaComments(sc.keyID, sc.paraID, '');
				}
			}
		},60000);
	}

	sc.nestedFiracClk = function(){
		if($('.firac-facts.commonsidewidth').hasClass('toggled')){
			sc.firacPageCount=0;
		}else{
			sc.firacPageCount=0;
			sc.firac={ownFirac:false, facts:'', issue:'', rationale:'', application:'', conclusion:'', comment:'', showCommentFirac:false};
			firacDetail();
		}
	}

	sc.changeFiracPage = function(turn){
		sc.firac.showCommentFirac=false;
		if(turn == 'prev'){
			if(sc.firacPageCount > 0){
				sc.firacPageCount--;
				sc.firacDetail=sc.firacList[sc.firacPageCount];
			}
		}else{
			if(sc.firacPageCount < sc.firacList.length-1){
				sc.firacPageCount++;
				sc.firacDetail=sc.firacList[sc.firacPageCount];
			}
		}
	}

	sc.addOwnFirac = function(){
		sc.firac={ownFirac:true, facts:'', issue:'', rationale:'', application:'', conclusion:'', firacNew:true, id:''};
	}

	sc.commentFiracStatus = function(){
		sc.firac.showCommentFirac=!sc.firac.showCommentFirac;
		sc.firac.comment='';
	}

	sc.commentFirac = function(){
		var firaqReq = {
						"comment":{
							"user_id":commonService.user_id,
							"date":new Date().getTime(),
							"comment_text":sc.firac.comment
						}
						,"firac_id":sc.firacDetail._id
					}

		docs.comment_on_firac(firaqReq).then(function(objS){
			if(objS.data.responseCode == 200){
				sc.firac.comment='';
				sc.firac.showCommentFirac=false;
				var index=objS.data.data.firacs.findIndex((x) => x._id == sc.firacDetail._id);
				if(index != -1){
					sc.firacDetail.comments = objS.data.data.firacs[index].comments;
				}
			}
		},function(objE){
			console.log('submit_firac => '+JSON.stringify(objE));
		})
	}

	sc.likeFirac = function(status){
		if(sc.firacDetail.like != status){
			var firaqReq = {
							"upvote_downvote":{
								"user_id":commonService.user_id,
								"date":new Date().getTime(),
								"positive":status
							},
							"firac_id":sc.firacDetail._id
						}

			docs.vote_on_firac(firaqReq).then(function(objS){
				if(objS.data.responseCode == 200){
					var index=objS.data.data.firacs.findIndex((x) => x._id == sc.firacDetail._id);
					if(index != -1){
						sc.firacDetail.likeCount=0;
						sc.firacDetail.disLikeCount=0;
						sc.firacDetail.like=undefined;
						for(var k=0;k<objS.data.data.firacs[index].votes.length;k++){
							if(objS.data.data.firacs[index].votes[k].positive)
								sc.firacDetail.likeCount++;
							else
								sc.firacDetail.disLikeCount++;
							if(objS.data.data.firacs[index].votes[k].user_id == commonService.user_id){
								sc.firacDetail.like=objS.data.data.firacs[index].votes[k].positive;
							}
						}
					}
				}
			},function(objE){
				console.log('submit_firac => '+JSON.stringify(objE));
			})
		}
	}

	sc.editFirac = function(){
		sc.firac={ownFirac:true,
			 		facts:sc.firacDetail.data.fact,
			   		issue:sc.firacDetail.data.issue,
				   	rationale:sc.firacDetail.data.rule,
					application:sc.firacDetail.data.analysis,
					conclusion:sc.firacDetail.data.conclusion,
					firacNew:false,
					id:sc.firacDetail._id
				};
	}

	sc.submitFirac = function(status){
		if(status){
			var firaqReq={
				"document_id": sc.keyID,
				"firac":{
					"user_id":commonService.user_id,
					"data":{
						"fact":sc.firac.facts,
						"issue":sc.firac.issue,
						"rule":sc.firac.rationale,
						"analysis":sc.firac.application,
						"conclusion":sc.firac.conclusion
					}
				}
			}
			if(!sc.firac.firacNew){
				firaqReq.firac_id = sc.firac.id;
				var end_point='edit_firacs';
			}else{
				var end_point='insert_firacs';
			}
			docs.submit_firac(firaqReq, end_point).then(function(objS){
				if(objS.data.responseCode == 200){
					sc.firac={ownFirac:false, facts:'', issue:'', rationale:'', application:'', conclusion:'', comment:'', showCommentFirac:false};
					firacDetail();
				}
			},function(objE){
				console.log('submit_firac => '+JSON.stringify(objE));
			})
		}else
			sc.firac={ownFirac:false, facts:'', issue:'', rationale:'', application:'', conclusion:'', comment:'', showCommentFirac:false};
	}

	function firacDetail(){
		docs.get_para_comments(sc.keyID).then(function(objS){
			sc.firacList=objS.data.data.firacs;
			sc.firacDetail={};
			for(var j=0;j<sc.firacList.length;j++){
				sc.firacList[j].likeCount=0;
				sc.firacList[j].disLikeCount=0;
				sc.firacList[j].like=undefined;
				for(var k=0;k<sc.firacList[j].votes.length;k++){
					if(sc.firacList[j].votes[k].positive)
						sc.firacList[j].likeCount++;
					else
						sc.firacList[j].disLikeCount++;
					if(sc.firacList[j].votes[k].user_id == commonService.user_id){
						sc.firacList[j].like=sc.firacList[j].votes[k].positive;
					}
				}
				if(j == sc.firacList.length-1){
					sc.firacDetail=sc.firacList[0];
				}
			}
		},function(objE){

		})
	}

	sc.likeAnnotation = function(e, docID, paraID, paragraphID, annotation, likeStatus){
		e.preventDefault();
		// sc.keyID=docID;
		// sc.paraID=paragraphID;
		if(annotation.like != likeStatus){
			var likeReq={"document_id":docID,"paragraphs_id":paraID,"annotations_id":annotation._id, "annotation_user_id":annotation.user_id.user_id, "votes":{"user_id":commonService.user_id,"date":new Date().getTime(),"positive":likeStatus}};
			docs.like_annotation(likeReq).then(function(objS){
				if(objS.data.responseCode == 200){
					// sc.sourceHandle?getParaComments(sc.keyID, paragraphID, 'emit'):getParaComments(sc.keyID, paragraphID, '');
					getParaComments(sc.keyID, paragraphID, 'emit');	
				}
			},function(objE){
				console.log('likeAnnotation => '+JSON.stringify(objE));
			})
		}
	}

	sc.replyAnnotation = function(docID, paraID, paragraphID, annotID, annotation){
		annotation.showAnnotReply?annotation.showAnnotReply=false:annotation.showAnnotReply=true;
		
		var commentReq={"document_id":docID,"paragraphs_id":paraID,"annotations_id": annotID,"comments": {"user_id": commonService.user_id,"date": new Date().getTime(),"comment_text": annotation.replyTextVal}};

		docs.comment_annotation(commentReq).then(function(objS){
			if(objS.data.responseCode == 200){
				sc.updateApiStatus=true;
				getParaComments(sc.keyID, paragraphID, '');
			}
		},function(objE){
			console.log('comment_annotation => '+JSON.stringify(objE));
		})
	}

	sc.replyStatusBtn = function(levelComment){
		levelComment.showAnnotReply?(levelComment.showAnnotReply=false,sc.updateApiStatus=true):(levelComment.showAnnotReply=true,sc.updateApiStatus=false);
		levelComment.replyTextVal='';
	}

	sc.commentComment = function(docID, paraID, paragraphID, annotID, commentID, comment){
		// console.log('docID => '+docID);
		// sc.keyID=docID;
		// sc.paraID=paragraphID;
		comment.showAnnotReply?comment.showAnnotReply=false:comment.showAnnotReply=true;
		var commentReq={"document_id":docID,"paragraphs_id":paraID,"annotations_id": annotID, "comment_ids":commentID, "comments": {"user_id": commonService.user_id,"date": new Date().getTime(),"comment_text": comment.replyTextVal}};

		docs.comment_comment(commentReq).then(function(objS){
			if(objS.data.responseCode == 200){
				sc.updateApiStatus=true;
				// getParaComments(sc.keyID, paragraphID, '');
				getParaComments(sc.keyID, paragraphID, 'emit');
			}
		},function(objE){
			console.log('comment_comment => '+JSON.stringify(objE));
		})
	}

	sc.notiIconClk = function(){
		sc.notiCont?!sc.notiCont:sc.notiCont=true;
		$('.dropdown.hover-line').toggleClass('open');
		if($('.dropdown.hover-line').hasClass('open')){
			getNotification();
		}
	}

	sc.readNoti = function(notification){
		if(notification==''){
			var notiReq={"user_id": commonService.user_id};
			if(sc.notiList.length>0)
				readNotification(notiReq, 'read_all_notification');
		}else{
			sc.$broadcast('openDocOnNoti', {tabName:'documentTab', id:notification.data.document_id});
			$('.dropdown.hover-line').toggleClass('open');
			var notiReq={"user_id": commonService.user_id, "notification_id":notification._id};
			if(!notification.read){
				readNotification(notiReq, 'read_notification');
			}
		}
	}
	
	getNotification();
	function getNotification(){
		docs.get_notifications({"user_id":commonService.user_id}).then(function(objS){
			if(objS.data.responseCode == 200){
				sc.notiList=[];
				if(objS.data.data!=null)
					sc.notiList = objS.data.data.notifications;
				sc.notiCnt=0;
				for(var i=0;i<sc.notiList.length;i++){
					if(!sc.notiList[i].read){
						sc.notiCnt++;
					}
				}
			}
		},function(objE){
			console.log('get_notifications => '+JSON.stringify(objE));
		})
	}

	function readNotification(notiReq, endPoint){
		docs.read_notification(notiReq, endPoint).then(function(objS){
			if(objS.data.responseCode == 200){
				sc.notiList=[];
				if(objS.data.data!=null)
					sc.notiList = objS.data.data.notifications;
				sc.notiCnt=0;
				for(var i=0;i<sc.notiList.length;i++){
					if(!sc.notiList[i].read){
						sc.notiCnt++;
					}
				}
			}
		},function(objE){
			console.log('readNotification => '+JSON.stringify(objE));
		})
	}

	sc.paraExpand = function(docID, paragraph, id){
		// console.log('id => '+$('#'+id).hasClass('in'));
		sc.keyID=docID;
		sc.paraID=paragraph.paragraph_id;
		if($('#'+id).hasClass('in'))
			sc.sourceHandle=false;
		else{
			sc.sourceHandle=true;
			sc.$broadcast('goToPara', { paraID: paragraph.paragraph_id});
		}
	}

	sc.upperMenuClk = function(type, index, page){
		if(type == 'logout'){
			if(storage.get('userLoginType') == 'normal'){
				logoutAPI();	
			}else{
				socialLoginService.logout();
			}
		}else
			sc.$broadcast('menuOpen', {name:type, index:index, page:page});
	}

	function logoutAPI(){
		user.logout_user({user_id:commonService.user_id}).then(function(objS){
			if(objS.data.responseCode == 200){
				localStorage.clear();
				commonService.user_id=null;
				commonService.user_info=null;
				commonService.review_card_arr=[];
				commonService.stateChangeVal='';
				commonService.tabReloadStatus=false;
				commonService.allTabsContent={search:[], document:[], case:[], graph:[], profile:[], article:[]};
				$state.go('landing_page.login');
			}
		},function(objE){
			console.log('readNotification => '+JSON.stringify(objE));
		})
	}

/***********Handled event when social logout success*************/
	$rootScope.$on('event:social-sign-out-success', function(event, logoutStatus){
		// console.log('social logout success => '+JSON.stringify(logoutStatus));
		logoutAPI();
    },function(err){
        console.log('social logout error => '+JSON.stringify(err));
    })

	sc.leftSideMenuClk = function(tab, index, page){
		if(tab == 'addCaseModal'){
			$('#addCaseFromSidebar').modal({backdrop: 'static'});
			sc.case={title:'', summary:'', search:''};
			sc.tempDocList=[];
			sc.searchDocList=[];
		}else{
			sc.$broadcast('menuOpen', {name:tab, index:index, page:page});
		}
	}

	sc.searchDoc = function(){
		docs.search_doc({title:sc.case.search}).then(function(objS){
			if(objS.data.responseCode == 200){
				sc.searchDocList=objS.data.data;
			}
		},function(objE){
			console.log('search_doc error => '+JSON.stringify(objE));
		})
	}

	sc.addRelevantDocs = function(doc, addStatus){
		if(addStatus){
			sc.tempDocList.push({id:doc._source.id, title:doc._source.title, courtName:doc._source.courtName});
		}else{
			var index=sc.tempDocList.findIndex((x) => x.id == doc.id);
			if(index!=-1){
				sc.tempDocList.splice(index,1);
			}
		}
	}

	sc.addCase = function(status){
		if(status){
			var docs=[];
			for(var i=0;i<sc.tempDocList.length;i++){
				docs.push(sc.tempDocList[i].id);
			}
			var req={"user_id":commonService.user_id, "document_list":docs, "case_title":sc.case.title, "case_description":sc.case.summary};
			if (!navigator.onLine) {
				alertify.error('Please check your network connection')
			} else if (req.case_title == '') {
				alertify.error("Please enter title")
			} else if (req.case_description == '') {
				alertify.error("Please enter summary")
			} else {
				$http({
					method: 'POST',
					url: API_URL+'documentActivity/add_case',
					data:req
				}).then(function(succ){
					if(succ.data.responseCode == 200){
						$('#addCaseFromSidebar').modal('hide');
					}
				},function(err){
					deff.reject(err);
				});
			}
		}else{
			$('#addCaseFromSidebar').modal('hide');	
		}
	}

	sc.searchListFilter = function(){
		var courtArr=[];
		var topicArr=[];
		var fromTime,toTime;
		$("#fromFilter").val()==""?fromTime=0:fromTime=$("#fromFilter").datepicker("getDate").getTime();
		$("#toFilter").val()==""?toTime=0:toTime=$("#toFilter").datepicker("getDate").getTime();
		for(var i=0;i<$(".court_filter").select2('data').length;i++){
			courtArr.push($(".court_filter").select2('data')[i].court_name);
		}
		for(var i=0;i<$(".topic_filter").select2('data').length;i++){
			topicArr.push($(".topic_filter").select2('data')[i].tag);
		}
		sc.$broadcast('filterSearchList', {id: sc.keyID, court:courtArr, topic:topicArr, from:fromTime, to:toTime, containing:sc.filter.containing, type:(sc.filter.type=='default'?'':sc.filter.type)});
	}

	$(document).ready(function() {
		
	});

	function filterCourt(){
		$(".court_filter").select2({
			placeholder: "Filter for court name",
			ajax: {
				url: API_URL+"documentActivity/court_auto_suggest",
				dataType: 'json',
				beforeSend: function (xhr) { 
					xhr.setRequestHeader("Authorization", storage.get('token'));
				},
				delay: 250,
				data: function (params) {
					return {
						suggestion: params.term
					};
				},
				processResults: function (data, params) {
				return {
						results: data.data
					};
				},
				cache: true
			},
			escapeMarkup: function (markup) { return markup; },
			minimumInputLength: 1,
			templateResult: formatRepoCourt,
			templateSelection: formatRepoSelectionCourt
		}).trigger('change');

		$(".topic_filter").select2({
			placeholder: "Filter for topic",
			ajax: {
				url: API_URL+"documentActivity/auto_suggest",
				dataType: 'json',
				beforeSend: function (xhr) { 
					xhr.setRequestHeader("Authorization", storage.get('token'));
				},
				delay: 250,
				data: function (params) {
					return {
						suggestion: params.term
					};
				},
				processResults: function (data, params) {
				return {
						results: data.data
					};
				},
				cache: true
			},
			escapeMarkup: function (markup) { return markup; },
			minimumInputLength: 1,
			templateResult: formatRepo,
			templateSelection: formatRepoSelection
		}).trigger('change');
	}

	function formatRepoCourt (repo) {
		if (repo.loading) return "Loading terms..";
			return "<div>"+repo.court_name+"</div>";
	}

	function formatRepoSelectionCourt (repo) {
		return repo.court_name;
	}

	function formatRepo (repo) {
		if (repo.loading) return "Loading terms..";
			return "<div>"+repo.tag+"</div>";
	}

	function formatRepoSelection (repo) {
		return repo.tag;
	}

/***********Avoiding the restrictions on url*************/
	sc.trustAsHtml = function(html) {
		return $sce.trustAsHtml(html);
	}

	sc.nestedSameDocClk = function(type){
		sc.similarDocList=[];
		if(type == 'doc'){
			if(!$('.filter-document-sidebar').hasClass('toggled')){
				similarListDoc();
			}
		}else{
			if(!$('.research-paper-sidebar').hasClass('toggled')){
				similarListDoc();
			}
		}
	}

	function similarListDoc(){
		docs.similar_docs(sc.keyID).then(function(objS){
			if(objS.data.responseCode == 200){
				sc.similarDocList=objS.data.data;
			}
		},function(objE){
			console.log('add annotation error => '+JSON.stringify(objE));
		})
	}

	var collapseArr;
	sc.nestedSameClk = function(type){
		sc.similarDocList=[];
		if(type == 'doc'){
			if(!$('.filter-document-sidebar').hasClass('toggled')){
				similarListArticle();
			}
		}else if(type == 'article'){
			if(!$('.research-paper-sidebar').hasClass('toggled')){
				similarListArticle();
			}
		}else if(type == 'comment'){
			if(!$('#comment-sidebar-block').hasClass('toggled')){
				get_all_article_comments();
				collapseArr=[];
			}
		}
	}

	function similarListArticle(){
		docs.similar_article_doc(sc.keyID).then(function(objS){
			sc.similarDocList=objS.data.data;
		},function(objE){
			console.log('similar article => '+JSON.stringify(objE));
		})
	}

	sc.nestedSameCaseClk = function(type){
		sc.similarDocList=[];
		if(type == 'article'){
			if(!$('.research-paper-sidebar').hasClass('toggled')){
				similarCaseArticle();
			}
		}
	}

	function similarCaseArticle(){
		docs.similar_case_article(sc.keyID).then(function(objS){
			sc.similarDocList=objS.data.data;
		},function(objE){
			console.log('similar article => '+JSON.stringify(objE));
		})
	}

	$(document).on('click', '.addinsightComm', function(e) {
		e.preventDefault();
		$(this).siblings(".firac-form").toggleClass("show");
		$timeout(function(){sc.user_comment='';},100);
	});

	sc.replyOnComment = function(commentId, commentText){ 
			var reply_req = {
			"draft_id": sc.keyID,
			"comment_ids":commentId,
			"comments": {
				"user_id": commonService.user_id,
				"user_name": commonService.user_info.profile.first_name + " " + commonService.user_info.profile.last_name,
				"iq_score": "245",
				"date": new Date().getTime(),
				"comment_text": commentText
			}
		}
		if (reply_req.comments.comment_text == "" || reply_req.comments.comment_text == undefined) {
			alertify.error("Please write something in comment")
		} else {
			docs.article_comments(reply_req).then(function(objS) {
				if (objS.data.responseCode == 200) {
					if(commentId.length == 0)
						$(".firac-form").toggleClass("show");
					sc.commentList = objS.data.data.comments;
					if(commentId.length>0){
						for(var j=0;j<collapseArr.length;j++){
							var index=sc.commentList.findIndex((x) => x._id == collapseArr[j]);
							if(index!=-1){
								sc.commentList[index].collapse=true;
							}
						}
					}
				} else {
					alertify.error("Server Error.")
				}
			}, function(objE) {

			})
		}
	}

	sc.collapseComment = function(id){
		if(!$('#'+id).hasClass('in')){
			collapseArr.push(id);
		}else{
			var index=collapseArr.findIndex((x) => x == id);
			collapseArr.splice(index, 1);
		}
	}

	function get_all_article_comments() {
		var req = { "draft_id": sc.keyID }
		docs.gel_all_article_comments(req).then(function(objS) {
			if (objS.data.responseCode == 200) {
				sc.commentList = objS.data.data;
				// sc.commentList[0].collapse=true;
			} else {
				alertify.error("Server Error.")
			}
		}, function(objE) {

		})
	}

	sc.replyArticleCommentStatusBtn = function(levelComment){
		levelComment.showCommReply?(levelComment.showCommReply=false):(levelComment.showCommReply=true);
		levelComment.replyTextVal='';
	}
	
	
}]);