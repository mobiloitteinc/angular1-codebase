'use strict';

angular.module('Lawcunae.login', [])

.controller('loginCtrl', ['$scope', 'user', 'storage', '$timeout', 'commonService', '$state', '$rootScope', '$location', 'alertify',function(sc, user, storage, $timeout, commonService, $state, $rootScope, $location, alertify) {
	
	sc.create={full_name:'',email:'',pwd:'',cnf_pwd:''};
	sc.login={email:'',pwd:'',valid_email:false,valid_pwd:false};
	sc.user={forgot_email:''};
	var emailRegx = /^[A-Z0-9_]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,3})+$/i;
	var nameRegx = /^[^'][a-zA-Z' ]*[^']$/;

/***********Login button click functionality*************/
	sc.loginClk = function(){
		if (!navigator.onLine) {
			alertify.error('Please check your network connection')
		} else if (sc.login.email == '') {
			alertify.error('Please enter your email')
		} else if (!emailRegx.test(sc.login.email)) {
			alertify.error('Please enter your valid email')
		} else if (sc.login.pwd == '') {
			alertify.error('Please enter your password')
		} else {
			commonService.showLoader();
			$('#loader').show();
			var data={
					"user_email": sc.login.email,
					"user_password": sc.login.pwd,
					"social_login":false,
					"browser_id": new Fingerprint().get()
					};
			user.login_user(data).then(function(objS){
				commonService.hideLoader();
				if(objS.data.responseCode == 200){
					storage.set('token',objS.data.data.token);
					storage.set('islogged',true);
					storage.set('user_id',objS.data.data.user_id);
					storage.set('userLoginType','normal');
					commonService.user_id=parseInt(storage.get('user_id'));
					$rootScope.userupdated=1;
					user.user_info(commonService.user_id);
					// commonService.ToShowAlert(objS.data.responseMessage);
					$state.go('side_menu.document_viewer.home');
				}else{
					alertify.alert(objS.data.responseMessage);
				}
			},function(objE){
				commonService.hideLoader();
				alertify.error('Server error');
				// console.log('login user objE => '+JSON.stringify(objE));
			});
		}
	}

/***********Getting started / signup button click functionality*************/
	sc.signupClk = function(){
		 if (!navigator.onLine) {
			alertify.error('Sorry there was network error')
		} else if (sc.create.full_name == '') {
			alertify.error('Please enter your full name')
		} else if (!nameRegx.test(sc.create.full_name)) {
			alertify.error('Please enter valid name')
		} else if (sc.create.email == '') {
			alertify.error('Please enter your email')
		} else if (!emailRegx.test(sc.create.email)) {
			alertify.error('Please enter your valid email')
		} else if (sc.create.pwd == '') {
			alertify.error('Please enter your password')
		} else if (sc.create.pwd != sc.create.cnf_pwd) {
			alertify.error('Password and confirm password does not match.')
		} else {
			commonService.showLoader();
			var data={
					"user_name": sc.create.full_name,
					"user_email": sc.create.email,
					"user_password": sc.create.pwd,
					"user_phone": ''
					};
			user.create_user(data).then(function(objS){
				commonService.hideLoader();
				if(objS.data.responseCode == 200){
					// sc.errMsg = '';
					// sc.succMsg = objS.data.responseMessage;
					sc.create={full_name:'',email:'',pwd:'',cnf_pwd:''};
					alertify.success(objS.data.responseMessage)
				}else{
					// sc.errMsg = objS.data.responseMessage;
					alertify.error(objS.data.responseMessage)
				}
			},function(objE){
				commonService.hideLoader();
				alertify.error('Server error')
				// console.log('create user objE => '+JSON.stringify(objE));
			});
		}
	}

/*************Functionality to blank the fields on click of particular role tab (login / signup)*************/
	sc.tabClk = function(role){
		if(role == 'login'){
			sc.create={full_name:'',email:'',pwd:'',cnf_pwd:''};
			sc.succMsg = '';
			sc.errMsg = '';
		}else
			sc.login={email:'',pwd:'',valid_email:false,valid_pwd:false};
	}

/******Keyup when click on enter button functionality******/
	sc.keyUp = function(e){
		if(emailRegx.test(sc.login.email))
			sc.login.valid_email=false;
		else
			sc.login.valid_email=true;
	}

/*****Open modal on click forgot password*******/
	sc.forgotPwdClk = function(){
		$timeout(function(){
			sc.user.forgot_email='';
			$('#forgot-pwd-modal .modal-backdrop').hide();
		},100);
	}

/***********Forgot password submit button click functionality*************/
	sc.forgotSubmitClk = function(){
		if (!navigator.onLine) {
			alertify.error('Please check your network connection')
		} else if (sc.user.forgot_email == '') {
			alertify.error('Please enter your email')
		} else if (!emailRegx.test(sc.user.forgot_email)) {
			alertify.error('Please enter your valid email')
		} else {
			var data={
					"user_email": sc.user.forgot_email
					};
			user.forgot_pwd_user(data).then(function(objS){
				if(objS.data.responseCode == 200){
					alertify.alert(objS.data.responseMessage);
					$('#forgot-pwd-modal').modal('hide');
				}else
					alertify.alert(objS.data.responseMessage);
			},function(objE){
				alertify.error('Server error')
			});
		}
	}

/******Search functionality from login screen******/
	sc.searchDocument = function(){
		commonService.searchWithoutLogin=sc.searchVal;
		$state.go('search_landing',{searchName:sc.searchVal});
	}
	
}]);